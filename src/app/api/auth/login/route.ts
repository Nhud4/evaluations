"use server";

import { NextResponse } from "next/server";
import { GetByUsername } from "@/sql/data/auth.data";
import { cookies } from "next/headers";
import bcrypt from "bcrypt";
import CryptoJS from "crypto-js";

const secretKey = process.env.SECRET_KEY;

export async function POST(req: Request) {
  try {
    const body = await req.text();
    const payload = JSON.parse(body);
    const cookie = cookies();

    const checkData = (await GetByUsername(payload.username)) as any;
    if (checkData?.length === 0) {
      return NextResponse.json(
        { message: "username tidak ditemukan" },
        { status: 404 }
      );
    }

    const compare = await bcrypt.compare(
      payload.password,
      checkData[0].password
    );
    if (!compare) {
      return NextResponse.json(
        { message: "username atau password salah" },
        { status: 422 }
      );
    }

    const token = CryptoJS.AES.encrypt(
      JSON.stringify(checkData[0]),
      secretKey as string
    ).toString();

    cookie.set("token", token);

    return NextResponse.json({ data: token }, { status: 201 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
