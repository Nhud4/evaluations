import { NextResponse } from "next/server";
import { cookies } from "next/headers";
import CryptoJS from "crypto-js";

const secretKey = process.env.SECRET_KEY;

export async function GET(req: Request) {
  try {
    const cookie = cookies();
    const token = cookie.get("token")?.value;
    const bytes = CryptoJS.AES.decrypt(
      token as string,
      secretKey as string
    ).toString(CryptoJS.enc.Utf8);

    const decryptedData = JSON.parse(bytes);

    return NextResponse.json({ data: decryptedData }, { status: 201 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
