import { NextResponse } from "next/server";
import { GetByUsername, InsertAuthData } from "@/sql/data/auth.data";
import bcrypt from "bcrypt";

export async function POST(request: Request) {
  try {
    const body = await request.text();
    const payload = JSON.parse(body);
    const checkData = (await GetByUsername(payload.username)) as any;

    if (checkData?.length > 0) {
      return NextResponse.json(
        { message: "username sudah ada" },
        { status: 422 }
      );
    }
    const hashedPassword = await bcrypt.hash(payload.password, 10);
    const insertData = await InsertAuthData({
      ...payload,
      password: hashedPassword,
    }).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ data: insertData }, { status: 201 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
