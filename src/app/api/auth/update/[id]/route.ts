"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetById, UpdateAuthData } from "@/sql/data/auth.data";
import bcrypt from "bcrypt";

export async function PUT(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const body = await req.text();
    const id = parseInt(query.split("/")[4], 10);
    const payload = JSON.parse(body);

    const checkData = (await GetById(id)) as any;
    if (checkData?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    if (payload.password) {
      const hashedPassword = await bcrypt.hash(payload.password, 10);
      payload.password = hashedPassword;
    }

    const result = await UpdateAuthData(payload, id).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
