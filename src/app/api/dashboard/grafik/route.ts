"use server";

import { NextResponse } from "next/server";
import { ListAllPerformance } from "@/sql/data/performance.data";

export async function GET(request: Request) {
  try {
    const result = (await ListAllPerformance()) as any;
    const data = {
      beast: { knn: "0", chi: "0", forward: "0", backward: "0" },
      prostate: { knn: "0", chi: "0", forward: "0", backward: "0" },
    };

    for (let i = 0; i < result.length; i++) {
      const element = result[i];
      if (element.data_type === "beast" && element.algo === "knn") {
        data.beast.knn = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "beast" && element.algo === "chi-square") {
        data.beast.chi = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "beast" && element.algo === "forward") {
        data.beast.forward = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "beast" && element.algo === "backward") {
        data.beast.backward = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "prostate" && element.algo === "knn") {
        data.prostate.knn = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "prostate" && element.algo === "chi-square") {
        data.prostate.chi = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "prostate" && element.algo === "forward") {
        data.prostate.forward = Number.parseFloat(element.accuracy).toFixed(2);
      }
      if (element.data_type === "prostate" && element.algo === "backward") {
        data.prostate.backward = Number.parseFloat(element.accuracy).toFixed(2);
      }
    }

    return NextResponse.json({ data }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
