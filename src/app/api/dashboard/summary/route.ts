"use server";

import { NextResponse } from "next/server";
import { CountAllBeast } from "@/sql/data/cancer.data";
import { CountAllProstate } from "@/sql/data/prostate.data";

export async function GET(request: Request) {
  try {
    const countBeast = (await CountAllBeast()) as any;
    const countProstate = (await CountAllProstate()) as any;

    const totalBeast = countBeast[0].total;
    const totalProstate = countProstate[0].total;

    const result = {
      total: totalBeast + totalProstate,
      beast: totalBeast,
      prostate: totalProstate,
    };
    return NextResponse.json({ data: result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
