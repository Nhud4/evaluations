"use server";

import { NextResponse } from "next/server";
import { InsertCancer } from "@/sql/data/cancer.data";
import { TestingCancerData } from "@/configs/cancerData";
import { KnnCancer, BackwardCancer, ForwardCancer } from "@/configs/algorithm";

export async function POST(req: Request) {
  try {
    const body = await req.text();
    const payload = JSON.parse(body);

    const random = (Math.random() + 1)
      .toString(36)
      .substring(2, 10)
      .toUpperCase();

    const knn = KnnCancer(TestingCancerData, payload);
    const chi_square = KnnCancer(TestingCancerData, payload);
    const backward = BackwardCancer(TestingCancerData, payload);
    const forward = ForwardCancer(TestingCancerData, payload);
    const code = `DBC-${random}`;
    const result = await InsertCancer({
      ...payload,
      code,
      knn,
      backward,
      forward,
      chi_square,
    }).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 201 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
