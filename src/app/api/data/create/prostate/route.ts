"use server";

import { NextResponse } from "next/server";
import { InsertProstate } from "@/sql/data/prostate.data";
import {
  KnnProstate,
  BackwardProstate,
  ForwardProstate,
} from "@/configs/algorithm";
import { TestingProstateData } from "@/configs/prostateData";

export async function POST(req: Request) {
  try {
    const body = await req.text();
    const payload = JSON.parse(body);

    const random = (Math.random() + 1)
      .toString(36)
      .substring(2, 10)
      .toUpperCase();

    const knn = KnnProstate(TestingProstateData, payload);
    const chi_square = KnnProstate(TestingProstateData, payload);
    const backward = BackwardProstate(TestingProstateData, payload);
    const forward = ForwardProstate(TestingProstateData, payload);

    const code = `DPC-${random}`;
    const result = await InsertProstate({
      ...payload,
      chi_square: chi_square,
      forward: forward,
      backward: backward,
      knn: knn,
      code,
    }).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 201 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
