"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetCancerByCode, DeleteCancer } from "@/sql/data/cancer.data";

export async function DELETE(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const payload = {
      code: query.split("/")[5],
    };

    const getData = (await GetCancerByCode(payload.code)) as any;

    if (getData?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    const result = await DeleteCancer(payload.code).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
