"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetCancerByCode } from "@/sql/data/cancer.data";

export async function GET(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const payload = {
      code: query.split("/")[5],
    };

    const getData = (await GetCancerByCode(payload.code)) as any;

    if (getData?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    return NextResponse.json({ data: getData[0] }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
