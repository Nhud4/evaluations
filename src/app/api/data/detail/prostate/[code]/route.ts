"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetProstateByCode } from "@/sql/data/prostate.data";

export async function GET(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const payload = {
      code: query.split("/")[5],
    };

    const result = (await GetProstateByCode(payload.code)) as any;

    if (result?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    return NextResponse.json({ data: result[0] }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
