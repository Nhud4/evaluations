"use server";

import { NextResponse, NextRequest } from "next/server";
import { QueryResultRow, sql } from "@vercel/postgres";
import { GetCancer, CountCancer } from "@/sql/data/cancer.data";

export async function GET(req: NextRequest) {
  try {
    const query = req.nextUrl.searchParams;
    const page = parseInt(query.get("page") as string, 10);
    const size = parseInt(query.get("size") as string, 10);
    const search = query.get("search") || "";

    const offset = (page - 1) * size;
    let data: QueryResultRow[] = [];
    let count = 0;
    let meta = {
      page: 1,
      totalPage: 0,
      totalData: 0,
      totalPerPage: 0,
    };

    const getData = (await GetCancer({ limit: size, offset, search })) as any;
    const rowCount = (await CountCancer({
      limit: size,
      offset,
      search,
    })) as any;

    if (getData.length > 0) {
      data = getData;
      count = rowCount[0]?.total;
    }

    meta = {
      page,
      totalPage: Math.ceil(count / size),
      totalData: count,
      totalPerPage: size > count ? count : size,
    };

    return NextResponse.json({ data, meta }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
