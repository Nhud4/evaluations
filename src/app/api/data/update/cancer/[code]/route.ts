"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetCancerByCode, UpdateCancer } from "@/sql/data/cancer.data";
import { TestingCancerData } from "@/configs/cancerData";
import { KnnCancer, BackwardCancer, ForwardCancer } from "@/configs/algorithm";

export async function PUT(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const body = await req.text();
    const code = query.split("/")[5];
    const payload = JSON.parse(body);

    const getData = (await GetCancerByCode(code)) as any;

    if (getData?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    const knn = KnnCancer(TestingCancerData, payload);
    const chi_square = KnnCancer(TestingCancerData, payload);
    const backward = BackwardCancer(TestingCancerData, payload);
    const forward = ForwardCancer(TestingCancerData, payload);

    console.log(knn, backward, forward, chi_square);

    const result = await UpdateCancer({
      ...payload,
      knn,
      backward,
      forward,
      chi_square,
      code,
    }).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
