"use server";

import { NextResponse, NextRequest } from "next/server";
import { GetProstateByCode, UpdateProstate } from "@/sql/data/prostate.data";
import {
  KnnProstate,
  BackwardProstate,
  ForwardProstate,
} from "@/configs/algorithm";
import { TestingProstateData } from "@/configs/prostateData";

export async function PUT(req: NextRequest) {
  try {
    const query = req.nextUrl.pathname;
    const body = await req.text();
    const code = query.split("/")[5];
    const payload = JSON.parse(body);

    const getData = (await GetProstateByCode(code)) as any;
    if (getData?.length === 0) {
      return NextResponse.json(
        { message: "data tidak ditemukan" },
        { status: 404 }
      );
    }

    const knn = KnnProstate(TestingProstateData, payload);
    const chi_square = KnnProstate(TestingProstateData, payload);
    const backward = BackwardProstate(TestingProstateData, payload);
    const forward = ForwardProstate(TestingProstateData, payload);

    const result = await UpdateProstate({
      ...payload,
      chi_square: chi_square,
      forward: forward,
      backward: backward,
      knn: knn,
      code,
    }).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
