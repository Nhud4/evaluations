"use server";

import { MigrationAuth } from "@/sql/auth";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  try {
    const result = await MigrationAuth();
    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
