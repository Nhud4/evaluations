"use server";

import { NextResponse } from "next/server";
import { InsertBulkCancer } from "@/sql/data/cancer.data";
import { TestingCancerData } from "@/configs/cancerData";
import { KnnCancer, BackwardCancer, ForwardCancer } from "@/configs/algorithm";

export async function GET(req: Request) {
  try {
    const data: any[] = [];

    for (let i = 0; i < TestingCancerData.length; i++) {
      const element = TestingCancerData[i];
      const random = (Math.random() + 1)
        .toString(36)
        .substring(2, 10)
        .toUpperCase();
      const code = `DBC-${random}`;

      const knn = KnnCancer(
        TestingCancerData,
        element as unknown as CancerPayload
      );
      const chi = KnnCancer(
        TestingCancerData,
        element as unknown as CancerPayload
      );
      const forward = ForwardCancer(
        TestingCancerData,
        element as unknown as CancerPayload
      );
      const backward = BackwardCancer(
        TestingCancerData,
        element as unknown as CancerPayload
      );

      data.push([
        code,
        element.diagnosis,
        `${element.radius_mean}`,
        `${element.texture_mean}`,
        `${element.perimeter_mean}`,
        `${element.area_mean}`,
        `${element.smoothness_mean}`,
        `${element.compactness_mean}`,
        `${element.concavity_mean}`,
        `${element.concave_points_mean}`,
        `${element.symmetry_mean}`,
        `${element.fractal_dimension_mean}`,
        `${element.radius_se}`,
        `${element.texture_se}`,
        `${element.perimeter_se}`,
        `${element.area_se}`,
        `${element.smoothness_se}`,
        `${element.compactness_se}`,
        `${element.concavity_se}`,
        `${element.concave_points_se}`,
        `${element.symmetry_se}`,
        `${element.fractal_dimension_se}`,
        `${element.radius_worst}`,
        `${element.texture_worst}`,
        `${element.perimeter_worst}`,
        `${element.area_worst}`,
        `${element.smoothness_worst}`,
        `${element.compactness_worst}`,
        `${element.concavity_worst}`,
        `${element.concave_points_worst}`,
        `${element.symmetry_worst}`,
        `${element.fractal_dimension_worst}`,
        chi,
        forward,
        backward,
        knn,
      ]);
    }

    // const result = await InsertBulkCancer(data).catch((error) => {
    //   return NextResponse.json({ error: error }, { status: 500 });
    // });

    return NextResponse.json({ data }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
