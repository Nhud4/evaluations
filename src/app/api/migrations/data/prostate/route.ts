"use server";

import { NextResponse } from "next/server";
import { InsertBulkProstate } from "@/sql/data/prostate.data";
import { TestingProstateData } from "@/configs/prostateData";
import {
  KnnProstate,
  BackwardProstate,
  ForwardProstate,
} from "@/configs/algorithm";

export async function GET(req: Request) {
  try {
    const data: any[] = [];

    for (let i = 0; i < TestingProstateData.length; i++) {
      const element = TestingProstateData[i];
      const random = (Math.random() + 1)
        .toString(36)
        .substring(2, 10)
        .toUpperCase();
      const code = `DPC-${random}`;

      const knn = KnnProstate(
        TestingProstateData,
        element as unknown as ProstatePayload
      );
      const chi = KnnProstate(
        TestingProstateData,
        element as unknown as ProstatePayload
      );
      const backward = BackwardProstate(
        TestingProstateData,
        element as unknown as ProstatePayload
      );
      const forward = ForwardProstate(
        TestingProstateData,
        element as unknown as ProstatePayload
      );
      data.push([
        code,
        element.diagnosis,
        `${element.radius}`,
        `${element.texture}`,
        `${element.perimeter}`,
        `${element.area}`,
        `${element.smoothness}`,
        `${element.compactness}`,
        `${element.symmetry}`,
        `${element.fractal_dimension}`,
        chi,
        forward,
        backward,
        knn,
      ]);
    }

    // const result = await InsertBulkProstate(data).catch((error) => {
    //   return NextResponse.json({ error: error }, { status: 500 });
    // });

    return NextResponse.json({ data }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
