"use server";

import { NextResponse } from "next/server";
import { MigrationPerformance } from "@/sql/performance";

export async function GET(request: Request) {
  try {
    const result = await MigrationPerformance();
    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
