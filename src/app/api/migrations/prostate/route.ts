"use server";

import { MigrationProstate } from "@/sql/prostate";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  try {
    const result = await MigrationProstate();
    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}
