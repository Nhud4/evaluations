"use server";

import { NextResponse } from "next/server";
import { GetPerformanceProstate } from "@/sql/data/prostate.data";
import { CountPerformance } from "@/configs/algorithm";
import { GetPerformanceBeast } from "@/sql/data/cancer.data";
import {
  DeletePerformance,
  InsertPerformance,
} from "@/sql/data/performance.data";

type PayloadData = {
  recall: number;
  precision: number;
  accuracy: number;
  f1Score: number;
};

export async function POST(req: Request) {
  try {
    const body = await req.text();
    const payload = JSON.parse(body);
    let knn: PayloadData = {
      recall: 0,
      precision: 0,
      accuracy: 0,
      f1Score: 0,
    };
    let chi: PayloadData = { recall: 0, precision: 0, accuracy: 0, f1Score: 0 };
    let forward: PayloadData = {
      recall: 0,
      precision: 0,
      accuracy: 0,
      f1Score: 0,
    };
    let backward: PayloadData = {
      recall: 0,
      precision: 0,
      accuracy: 0,
      f1Score: 0,
    };

    if (payload.dataType === "prostate") {
      const data = (await GetPerformanceProstate()) as any[];
      const dataKnn = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.knn,
      }));
      const dataChi = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.chi_square,
      }));
      const dataForward = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.forward,
      }));
      const dataBackward = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.backward,
      }));

      knn = CountPerformance(dataKnn);
      chi = CountPerformance(dataChi);
      forward = CountPerformance(dataForward);
      backward = CountPerformance(dataBackward);
    }

    if (payload.dataType === "beast") {
      const data = (await GetPerformanceBeast()) as any[];
      const dataKnn = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.knn,
      }));
      const dataChi = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.chi_square,
      }));
      const dataForward = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.forward,
      }));
      const dataBackward = data.map((item) => ({
        code: item.code,
        actual: item.diagnosis,
        prediction: item.backward,
      }));

      knn = CountPerformance(dataKnn);
      chi = CountPerformance(dataChi);
      forward = CountPerformance(dataForward);
      backward = CountPerformance(dataBackward);
    }

    await DeletePerformance(payload.dataType);

    const data = [
      [
        payload.dataType,
        "knn",
        `${knn.recall}`,
        `${knn.precision}`,
        `${knn.accuracy}`,
        `${knn.f1Score}`,
      ],
      [
        payload.dataType,
        "chi-square",
        `${chi.recall}`,
        `${chi.precision}`,
        `${chi.accuracy}`,
        `${chi.f1Score}`,
      ],
      [
        payload.dataType,
        "forward",
        `${forward.recall}`,
        `${forward.precision}`,
        `${forward.accuracy}`,
        `${forward.f1Score}`,
      ],
      [
        payload.dataType,
        "backward",
        `${backward.recall}`,
        `${backward.precision}`,
        `${backward.accuracy}`,
        `${backward.f1Score}`,
      ],
    ];

    const result = await InsertPerformance(data).catch((error) => {
      return NextResponse.json({ error: error }, { status: 500 });
    });

    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
