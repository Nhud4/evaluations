"use server";

import { NextResponse, NextRequest } from "next/server";
import { ListPerformance } from "@/sql/data/performance.data";

export async function GET(req: NextRequest) {
  try {
    const query = req.nextUrl.searchParams;
    const dataType = query.get("dataType") || "";

    const getData = (await ListPerformance(dataType)) as any;
    const data = getData.map((item: any) => ({
      ...item,
      accuracy: Number.parseFloat(item.accuracy).toFixed(2),
    }));
    return NextResponse.json({ data }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
