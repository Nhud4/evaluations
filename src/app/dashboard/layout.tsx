"use client";

import React, { useContext } from "react";
import SideBar from "@/components/SideBar";
import Navigation from "@/components/Navigations";
import { ShowContext } from "@/context/ShowContext/ShowContext";

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const { show } = useContext(ShowContext);
  return (
    <section>
      <SideBar />
      <Navigation mainMenu="Dashboard" />
      <main
        className={[
          "transition-all duration-500 px-4 pt-2",
          show.open ? "ml-64" : "ml-20",
        ].join(" ")}
      >
        {children}
      </main>
    </section>
  );
}
