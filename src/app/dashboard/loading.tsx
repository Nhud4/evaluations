"use client";

import Image from "next/image";
import ASSETS_IMAGES from "@/configs/images";

export default function Loading() {
  return (
    <main className="flex flex-col justify-center items-center h-screen">
      <Image alt="loading" src={ASSETS_IMAGES.Loading} />
      <h1 className="text-2xl font-semibold">Loading...</h1>
    </main>
  );
}
