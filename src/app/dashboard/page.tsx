"use client";

import React from "react";
import Summary from "@/feature/dashboard/Summary";
import BarChart from "@/components/BarChart";
import Evaluation from "@/feature/dashboard/Evaluation";

export default function Dashboard() {
  return (
    <>
      <Summary />
      <div className="grid grid-cols-3 gap-4 mt-8">
        <BarChart className="col-span-2 h-fit" />
        <Evaluation />
      </div>
    </>
  );
}
