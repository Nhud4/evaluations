import type { Metadata } from "next";
import { Poppins } from "next/font/google";
import "./globals.css";
import ContextProvider from "@/context";
import QueryProvider from "./QueryProvider";

const inter = Poppins({
  subsets: ["latin"],
  weight: "400",
});

export const metadata: Metadata = {
  title: "Aplikasi Evaluasi KNN",
  description: "Aplikasi Evaluasi KNN",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <QueryProvider>
      <html lang="en">
        <body className={inter.className}>
          <ContextProvider>{children}</ContextProvider>
        </body>
      </html>
    </QueryProvider>
  );
}
