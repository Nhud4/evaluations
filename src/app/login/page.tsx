"use client";

import React from "react";
import FormLogin from "@/feature/login";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Login() {
  return (
    <main>
      <FormLogin />
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </main>
  );
}
