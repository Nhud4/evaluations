"use client";

import React from "react";
import TablePerformance from "@/feature/performance/TablePerformance";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Performance() {
  return (
    <>
      <TablePerformance />
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </>
  );
}
