"use client";

import React from "react";
import FormCancer from "@/form/FormCancer";

export default function AddCancer() {
  return (
    <main>
      <FormCancer formType="add" />
    </main>
  );
}
