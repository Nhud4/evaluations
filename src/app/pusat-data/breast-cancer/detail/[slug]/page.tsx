"use client";

import React, { useMemo } from "react";
import { useParams } from "next/navigation";
import { useQuery } from "@tanstack/react-query";

export default function DetailCancer() {
  const params = useParams()["slug"];

  const { data } = useQuery({
    queryKey: ["/cancer/detail", [params]],
    queryFn: async () => {
      const res = await fetch(`/api/data/detail/cancer/${params}`, {
        method: "GET",
      });
      const responseData = await res.json();
      return responseData;
    },
  });

  const newData = useMemo(() => {
    const sectionOne = [
      { label: "Radius Mean", value: data?.data.radius_mean },
      { label: "Texture Mean", value: data?.data.texture_mean },
      { label: "Perimeter Mean", value: data?.data.perimeter_mean },
      { label: "Area Mean", value: data?.data.area_mean },
      { label: "Smoothness Mean", value: data?.data.smoothness_mean },
      { label: "compactness Mean", value: data?.data.compactness_mean },
      { label: "Concavity Mean", value: data?.data.concavity_mean },
      { label: "concave points mean", value: data?.data.concave_points_mean },
      { label: "symmetry mean", value: data?.data.symmetry_mean },
      {
        label: "fractal dimension mean",
        value: data?.data.fractal_dimension_mean,
      },
    ];

    const sectionTwo = [
      { label: "radius se", value: data?.data.radius_se },
      { label: "texture se", value: data?.data.texture_se },
      { label: "perimeter se", value: data?.data.perimeter_se },
      { label: "area se", value: data?.data.area_se },
      { label: "smoothness se", value: data?.data.smoothness_se },
      { label: "compactness se", value: data?.data.compactness_se },
      { label: "concavity se", value: data?.data.concavity_se },
      { label: "concave points se", value: data?.data.concave_points_se },
      { label: "symmetry se", value: data?.data.symmetry_se },
      { label: "fractal dimension se", value: data?.data.fractal_dimension_se },
    ];

    const sectionThree = [
      { label: "radius worst", value: data?.data.radius_worst },
      { label: "texture worst", value: data?.data.texture_worst },
      { label: "perimeter worst", value: data?.data.perimeter_worst },
      { label: "area worst", value: data?.data.area_worst },
      { label: "smoothness worst", value: data?.data.smoothness_worst },
      { label: "compactness worst", value: data?.data.compactness_worst },
      { label: "concavity worst", value: data?.data.concavity_worst },
      { label: "concave points worst", value: data?.data.concave_points_worst },
      { label: "symmetry worst", value: data?.data.symmetry_worst },
      {
        label: "fractal dimension worst",
        value: data?.data.fractal_dimension_worst,
      },
    ];
    return [sectionOne, sectionTwo, sectionThree];
  }, [data]);

  const topSection = useMemo(() => {
    const sectionTop = [
      { label: "Diagnosis", value: data?.data.diagnosis },
      { label: "KNN", value: data?.data.knn },
      { label: "Chi Square", value: data?.data.chi_square },
      { label: "Forward", value: data?.data.forward },
      { label: "Backward", value: data?.data.backward },
    ];
    return sectionTop;
  }, [data]);

  return (
    <div className="bg-white p-4 rounded-xl">
      <div className="flex justify-between items-center">
        <div className="flex items-center space-x-1">
          <h1 className="font-semibold">Detail Beast Cancer -</h1>
          <h2 className="font-semibold text-neutral-5">{params}</h2>
        </div>
        <div className="flex items-center space-x-2">
          {topSection.map((item, index) => (
            <div
              key={index}
              className="flex justify-center items-center space-x-1 bg-secondary-6 px-4 py-2 rounded-lg"
            >
              <h1 className="text-xs text-white">{item.label}:</h1>
              <p className="text-xs font-semibold text-white">{item.value}</p>
            </div>
          ))}
        </div>
      </div>
      <div className="space-y-2 mt-2">
        {newData.map((item, index) => (
          <div
            key={index}
            className="grid grid-cols-4 gap-2 border border-neutral-3 rounded-lg p-3"
          >
            {item.map((val, key) => (
              <div key={key}>
                <h3 className="text-sm text-neutral-5 capitalize">
                  {val.label}
                </h3>
                <p className="text-sm">{val.value}</p>
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}
