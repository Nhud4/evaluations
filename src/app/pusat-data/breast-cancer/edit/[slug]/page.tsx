"use client";

import React from "react";
import FormCancer from "@/form/FormCancer";

export default function EditCancer() {
  return (
    <main>
      <FormCancer formType="edit" />
    </main>
  );
}
