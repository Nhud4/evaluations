"use client";

import React from "react";
import CancerTable from "@/feature/pusatData/TableCancer";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function BreastCancer() {
  return (
    <main>
      <CancerTable />
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </main>
  );
}
