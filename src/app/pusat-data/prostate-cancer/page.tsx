"use client";

import React from "react";
import TableApple from "@/feature/pusatData/TableApple";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function AppleQuality() {
  return (
    <div>
      <TableApple />
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </div>
  );
}
