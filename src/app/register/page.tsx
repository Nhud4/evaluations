"use client";

import React from "react";
import FormRegister from "@/feature/register";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Register() {
  return (
    <main>
      <FormRegister />
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </main>
  );
}
