"use client";

import React, { useEffect, useState } from "react";
import { useQuery } from "@tanstack/react-query";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  ChartOptions,
  ChartData,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip);

const options = {
  responsive: true,
  onHover: (event, element) => {
    const canvas: any = event.native?.target;
    canvas.style.cursor = element[0] ? "pointer" : "default";
  },
  plugins: {
    legend: {
      display: false,
    },
  },
  layout: {
    padding: {
      top: 40,
    },
  },
  scales: {
    y: {
      grid: {
        borderDash: [50],
      },
    },
    x: {
      grid: {
        display: false,
      },
    },
  },
} as ChartOptions<"bar">;

type Props = {
  className?: string;
};

export const BarChart: React.FC<Props> = ({ className }) => {
  const [trigger, setTrigger] = useState("0");
  const { data } = useQuery({
    queryKey: ["dashboard/grafik", [trigger]],
    queryFn: async () => {
      const res = await fetch("/api/dashboard/grafik", { method: "GET" });
      const { data } = await res.json();
      return data;
    },
  });

  useEffect(() => {
    setTrigger(trigger + 0);
  }, []);

  const chartData = {
    labels: [
      "Algoritma K-NN",
      "Forward Selection",
      "Backward Elimination",
      "Chi-Square",
    ],
    datasets: [
      {
        label: "Breast Cancer",
        data: [
          data?.beast?.knn,
          data?.beast?.forward,
          data?.beast?.backward,
          data?.beast?.chi,
        ],
        backgroundColor: "#B8646B",
        borderRadius: 15,
        borderSkipped: false,
        barPercentage: 0.75,
      },
      {
        label: "Prostate Cancer",
        data: [
          data?.prostate?.knn,
          data?.prostate?.forward,
          data?.prostate?.backward,
          data?.prostate?.chi,
        ],
        backgroundColor: "#B3A742",
        borderRadius: 15,
        borderSkipped: false,
        barPercentage: 0.75,
      },
    ],
  } as ChartData<"bar", number[], string>;

  return (
    <div
      className={[className, "bg-white rounded-xl p-4 drop-shadow-xl"].join(
        " "
      )}
    >
      <div className="flex justify-between items-center">
        <h1 className="text-lg text-primary font-semibold">
          Grafik Akurasi ALgortima
        </h1>
        <div className="flex items-center space-x-4">
          <div className="flex items-center space-x-2">
            <div className="w-5 h-5 bg-secondary-5 rounded-lg" />
            <h2 className="text-sm text-secondary-1">Breast Cancer</h2>
          </div>
          <div className="flex items-center space-x-2">
            <div className="w-5 h-5 bg-secondary-6 rounded-lg" />
            <h2 className="text-sm text-secondary-1">Prostate Cancer</h2>
          </div>
        </div>
      </div>
      <Bar options={options} data={chartData} />
    </div>
  );
};
