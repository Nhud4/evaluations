"use client";

import React from "react";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";

type Props = {
  open: boolean;
  onClose: () => void;
  title?: string;
  children: React.ReactNode;
};

export const BaseModal: React.FC<Props> = ({
  open,
  onClose,
  title,
  children,
}) => {
  if (open) {
    return (
      <div className="absolute inset-0 z-50 flex items-center justify-center bg-opacity-25 bg-neutral-5">
        <div className="min-w-[400px] py-4 bg-white rounded-md">
          {title ? (
            <div className="flex items-center px-4 pb-4 border-b border-neutral">
              <button onClick={onClose}>
                <Image alt="icon" src={ASSETS_ICONS.Close} />
              </button>
              <h5 className="mt-1 ml-2 font-semibold">{title}</h5>
            </div>
          ) : null}
          <div className="px-4 mt-4">{children}</div>
        </div>
      </div>
    );
  }
};
