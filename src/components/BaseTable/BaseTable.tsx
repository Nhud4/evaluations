"use client";

import React, { HTMLAttributes, useEffect, useMemo, useState } from "react";
import DataTable, { TableColumn } from "react-data-table-component";
import { tableStyles } from "@/configs/dataTable";
import ASSETS_ICONS from "@/configs/icons";
import ASSETS_IMAGES from "@/configs/images";
import Image from "next/image";
import Select, { StylesConfig } from "react-select";
import { useDebounce } from "@/configs/utils";

import styles from "./styles.module.css";

const options = [
  { value: 10, label: 10 },
  { value: 15, label: 15 },
  { value: 20, label: 20 },
  { value: 50, label: 50 },
  { value: 100, label: 100 },
];

const customStyles: StylesConfig = {
  option: (provided) => ({
    ...provided,
    fontSize: 12,
    padding: 5,
  }),
  control: () => ({
    display: "flex",
    width: 40,
  }),
  valueContainer: (provided) => ({
    ...provided,
    padding: 0,
  }),
  indicatorSeparator: () => ({
    display: "none",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "8px 0",
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...provided, opacity, transition, fontSize: 12 };
  },
};

type Props<T> = {
  columns: TableColumn<T>[];
  data: T[];
  resetSort?: boolean;
  isLoading?: boolean;
  actionComponent?: React.ReactElement;
  meta?: PaginationMeta;
  title?: string;
  onSearch?: (search: string) => void;
  onChangeRowPerPage?: (page: number, size: number) => void;
  onChangePage?: (page: number) => void;
  headerClassName?: string;
  summary?: React.ReactElement;
} & Pick<HTMLAttributes<HTMLDivElement>, "className" | "style">;

export const BaseTable: <T>(props: Props<T>) => React.ReactElement = ({
  columns,
  data,
  resetSort,
  isLoading,
  actionComponent,
  meta,
  title,
  onSearch,
  onChangeRowPerPage,
  onChangePage,
  headerClassName,
  summary,
}) => {
  const [search, setSearch] = useState<string | null>(null);
  const debounceSearch = useDebounce(search, 500);
  const showingInfo = useMemo(() => {
    const start =
      Number(meta?.page) === 1
        ? 1
        : Number(meta?.totalPerPage) + (Number(meta?.page) - 1);
    const end = Number(meta?.page) * Number(meta?.totalPerPage);

    return Number(meta?.totalData)
      ? `${start}-${
          end > Number(meta?.totalData) ? Number(meta?.totalData) : end
        }`
      : "0";
  }, [meta]);

  useEffect(() => {
    if (onSearch) {
      onSearch(debounceSearch as string);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debounceSearch]);

  return (
    <div>
      {title ? <h1 className="text-xl font-semibold">{title}</h1> : null}
      {summary ? <>{summary}</> : null}
      <div className={styles.main}>
        <div className="pb-4 border-b border-neutral-3">
          <div
            className={[
              "flex justify-between items-center px-4",
              headerClassName,
            ].join(" ")}
          >
            {onSearch ? (
              <div className={styles.search}>
                <Image alt="icons" src={ASSETS_ICONS.Search} />
                <input
                  className="text-sm"
                  placeholder="Cari berdasarkan id..."
                  type="text"
                  onChange={({ target: { value } }) => setSearch(value)}
                  value={search as string}
                />
              </div>
            ) : null}
            {actionComponent && <div>{actionComponent}</div>}
          </div>
        </div>
        <div
          className={[styles.container, isLoading ? styles.loading : ""].join(
            " "
          )}
        >
          <DataTable
            columns={columns}
            data={isLoading ? new Array(10).fill({}) : data}
            customStyles={tableStyles(resetSort)}
            fixedHeader
            fixedHeaderScrollHeight={"400px"}
            defaultSortAsc={false}
            theme="solarized"
            noDataComponent={
              <div className="grid w-full min-h-full bg-white place-items-center">
                <div className="flex flex-col items-center">
                  <Image alt="empty" src={ASSETS_IMAGES.Empty} />
                  <p className="font-semibold text-center">
                    Data tidak ditemukan.
                  </p>
                </div>
              </div>
            }
          />
        </div>
        {meta ? (
          <div className={styles.pagination}>
            <div>
              <p>
                {showingInfo} dari {meta?.totalData}
              </p>
            </div>
            <div className="flex items-center space-x-4">
              <div className="border border-neutral-2 px-2 rounded-md">
                <Select
                  defaultValue={options[0]}
                  menuPlacement="auto"
                  options={options}
                  styles={customStyles}
                  onChange={(newValue) => {
                    const option = newValue as Option;
                    const currentPage = meta?.page as number;
                    onChangeRowPerPage &&
                      onChangeRowPerPage(currentPage, Number(option.value));
                  }}
                />
              </div>
              <div className="flex items-center space-x-2">
                <button
                  className={[
                    "transform rotate-90",
                    meta?.page === 1 && "pointer-events-none opacity-60",
                  ].join(" ")}
                  disabled={meta?.page === 1}
                  onClick={() => {
                    const currentPage = meta?.page as number;
                    if (onChangePage) {
                      onChangePage(currentPage - 1);
                    }
                  }}
                >
                  <Image alt="icons" src={ASSETS_ICONS.ChevronBlack} />
                </button>
                <div className="flex items-center space-x-1">
                  <p className="text-xs font-medium">{meta?.page}</p>
                  <p className="text-xs font-medium">/</p>
                  <p className="text-xs font-medium opacity-50">
                    {meta?.totalPage}
                  </p>
                </div>
                <button
                  className={[
                    "transform -rotate-90",
                    meta?.page === meta?.totalPage &&
                      "pointer-events-none opacity-60",
                  ].join(" ")}
                  disabled={meta?.page === meta?.totalPage}
                  onClick={() => {
                    const currentPage = meta?.page as number;
                    if (onChangePage) {
                      onChangePage(currentPage + 1);
                    }
                  }}
                >
                  <Image alt="icons" src={ASSETS_ICONS.ChevronBlack} />
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};
