"use client";

import React from "react";
import { usePathname } from "next/navigation";

type Props = {
  mainMenu: string;
  subMenu?: string;
};

export const BreadCamb: React.FC<Props> = ({ mainMenu, subMenu }) => {
  const path = usePathname();
  const splitPath = path.split("/");

  return (
    <div className="flex items-center space-x-2 pl-3 py-2">
      <h1 className="text-sm capitalize">{mainMenu}</h1>
      {subMenu ? (
        <>
          <p className="text-xs">/</p>
          <p
            className={[
              "text-sm capitalize",
              splitPath.length >= 4 ? "" : "text-secondary-1",
            ].join(" ")}
          >
            {subMenu}
          </p>
          {splitPath.length >= 4 ? (
            <>
              <p className="text-sm">/</p>
              <p
                className={[
                  "text-sm capitalize",
                  splitPath.length >= 5 ? "" : "text-secondary-1",
                ].join(" ")}
              >
                {splitPath[3]}
              </p>
            </>
          ) : null}
          {splitPath.length >= 5 ? (
            <>
              <p className="text-sm">/</p>
              <p className="text-sm capitalize text-secondary-1">
                {splitPath[4]}
              </p>
            </>
          ) : null}
        </>
      ) : null}
    </div>
  );
};
