"use client";

import React, { useState } from "react";

type Props = {
  children: React.ReactNode;
  type?: "button" | "submit" | "reset";
  onClick?: () => void;
  isDisabled?: boolean;
  className?: string;
};

export const Button: React.FC<Props> = ({
  children,
  type,
  onClick,
  isDisabled,
  className,
}) => {
  return (
    <button
      type={type ? type : "button"}
      className={[
        "text-white w-full p-2 rounded-lg",
        className,
        isDisabled
          ? "bg-neutral-3 border-neutral-3"
          : "bg-primary border-primary",
      ].join(" ")}
      onClick={onClick}
      disabled={isDisabled}
    >
      {children}
    </button>
  );
};
