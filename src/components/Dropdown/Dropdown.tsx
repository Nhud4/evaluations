"use client";

import React from "react";
import Select from "react-select";
import { SingleValue } from "react-select";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

type Props = {
  defaultValue?: { value: string | number | boolean; label: string };
  name: string;
  options?: { value: string | number | boolean; label: string }[];
  label?: string;
  placeholder?: string;
  isClearable?: boolean;
  value?: { value: string | number | boolean; label: string }[];
  onChange?: (
    values: SingleValue<{ value: string | number | boolean; label: string }>
  ) => void;
  isDisabled?: boolean;
  maxMenuHeight?: number;
  isLoading?: boolean;
};

export const Dropdown: React.FC<Props> = ({
  defaultValue,
  name,
  options,
  label,
  placeholder,
  isClearable = true,
  onChange,
  isDisabled,
  maxMenuHeight,
  value,
  isLoading,
}) => {
  return (
    <div>
      {label ? (
        <h1
          className={[
            "text-sm font-semibold",
            isLoading ? "pb-1" : "pb-2",
          ].join(" ")}
        >
          {label}
        </h1>
      ) : null}
      {isLoading ? (
        <Skeleton height={40} />
      ) : (
        <Select
          value={value}
          maxMenuHeight={maxMenuHeight}
          isDisabled={isDisabled}
          className="capitalize"
          options={options}
          name={name}
          defaultValue={defaultValue}
          placeholder={placeholder}
          isClearable={isClearable}
          onChange={(choice) => {
            if (onChange) {
              onChange(choice);
            }
          }}
          styles={{
            menu: (rest) => ({ ...rest, zIndex: 9999 }),
            control: (rest) => ({ ...rest, cursor: "pointer", minWidth: 175 }),
            option: (rest) => ({ ...rest, cursor: "pointer" }),
          }}
        />
      )}
    </div>
  );
};
