"use client";

import React, { useContext, useEffect, useMemo, useState } from "react";
import ASSETS_ICONS from "@/configs/icons";
import ASSETS_IMAGES from "@/configs/images";
import Image from "next/image";
import { TIME_MANAGE } from "@/configs/dummy";
import Link from "next/link";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import FormSetting from "@/feature/setting";
import { useQuery } from "@tanstack/react-query";
import { ShowContext } from "@/context/ShowContext/ShowContext";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);
  const [openSidebar, setOpenSidebar] = useState(true);
  const hour = new Date().getHours();
  const { setModal, onClose } = useContext(ModalContext);
  const { setShow } = useContext(ShowContext);
  const [trigger, setTrigger] = useState("0");

  const { data, isLoading } = useQuery({
    queryKey: ["auth/profile", [trigger]],
    queryFn: async () => {
      const res = await fetch("/api/auth/profile", { method: "GET" });
      return await res.json();
    },
  });

  const time = useMemo(() => {
    let value = "";
    let icon = null;
    if (TIME_MANAGE.pagi.includes(hour)) {
      value = "Pagi";
      icon = ASSETS_IMAGES.Morning;
    }
    if (TIME_MANAGE.siang.includes(hour)) {
      value = "Siang";
      icon = ASSETS_IMAGES.Day;
    }
    if (TIME_MANAGE.sore.includes(hour)) {
      value = "Sore";
      icon = ASSETS_IMAGES.Afternoon;
    }
    if (TIME_MANAGE.petang.includes(hour)) {
      value = "Petang";
      icon = ASSETS_IMAGES.Evening;
    }
    if (TIME_MANAGE.malam.includes(hour)) {
      value = "Malam";
      icon = ASSETS_IMAGES.Night;
    }

    return { value, icon };
  }, [hour]);

  const handleSuccess = () => {
    onClose();
    setOpenMenu(false);
    setTrigger(trigger + 0);
  };

  const onSetting = () => {
    setModal({
      open: true,
      content: <FormSetting handleSuccess={handleSuccess} />,
      title: "Informasi akun",
    });
  };

  useEffect(() => {
    setShow({
      open: openSidebar,
    });
  }, [openSidebar]);

  return (
    <>
      <div className="flex justify-between items-center bg-white p-3 pr-6">
        <div className="flex items-center space-x-3">
          <button onClick={() => setOpenSidebar(!openSidebar)}>
            <Image alt="icons" src={ASSETS_ICONS.Menu} />
          </button>
          <div>
            <div className="flex items-center space-x-2">
              <p className="text-secondary-1 text-sm">Seamat {time.value},</p>
              {time.icon ? (
                <Image alt="icon" src={time.icon} className="w-6 h-6" />
              ) : null}
            </div>
            {isLoading ? (
              <Skeleton className="!z-0" width={100} />
            ) : (
              <p className="text-lg font-medium text-black capitalize">
                {data?.data?.name}
              </p>
            )}
          </div>
        </div>
        <div className="flex items-center space-x-4">
          <Image alt="image" src={ASSETS_IMAGES.Avatar} />
          <button
            onClick={() => setOpenMenu(!openMenu)}
            className={openMenu ? "rotate-180" : "rotate-0"}
          >
            <Image alt="icons" src={ASSETS_ICONS.ChevronBlack} />
          </button>
        </div>
      </div>
      {openMenu ? (
        <div className="absolute right-4 mt-1 bg-white py-2 rounded-lg grid grid-cols-1 divide-y divide-secondary-3 drop-shadow-xl z-50">
          <button
            className="flex items-center space-x-2 px-3 pb-2"
            onClick={onSetting}
          >
            <Image alt="icons" src={ASSETS_ICONS.User} />
            <p>Pengaturan Akun</p>
          </button>
          <Link
            href={"/login"}
            className="flex items-center space-x-2 px-3 pt-2"
          >
            <Image alt="icons" src={ASSETS_ICONS.LogOut} />
            <p>Log Out</p>
          </Link>
        </div>
      ) : null}
    </>
  );
};
