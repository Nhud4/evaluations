"use client";

import React from "react";

type Props = {
  data: number;
};

export const LineProcess: React.FC<Props> = ({ data }) => {
  return (
    <div>
      <h1 className="text-sm text-end text-primary font-semibold">{data}%</h1>
      <div className="relative h-[6px] bg-neutral-4 rounded-full">
        <div
          className="absolute h-[6px] bg-primary rounded-full"
          style={{ width: `${data}%` }}
        />
      </div>
    </div>
  );
};
