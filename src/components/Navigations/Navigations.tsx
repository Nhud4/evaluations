"use client";

import React, { useContext } from "react";
import Header from "../Header";
import BreadCamb from "../BreadCamb";
import { ShowContext } from "@/context/ShowContext/ShowContext";

type Props = {
  mainMenu: string;
  subMenu?: string;
};

export const Navigation: React.FC<Props> = ({ mainMenu, subMenu }) => {
  const { show } = useContext(ShowContext);
  return (
    <div
      className={[
        "transition-all duration-500",
        show.open ? "pl-64" : "pl-20",
      ].join(" ")}
    >
      <Header />
      <BreadCamb mainMenu={mainMenu} subMenu={subMenu} />
    </div>
  );
};
