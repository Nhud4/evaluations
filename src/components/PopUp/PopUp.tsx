"use client";

import React, { useState, useRef, HTMLAttributes, useEffect } from "react";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";

type Props = {
  actions?: Actions[];
  onDetail?: () => void;
  onEdit?: () => void;
  onDelete?: () => void;
};

export const PopUp = ({ actions, onDetail, onEdit, onDelete }: Props) => {
  const [isOpen, setIsOpen] = useState(false);
  const Popupref = useRef<HTMLDivElement>(null);
  const openModal = () => {
    setIsOpen(!isOpen);
  };
  const CloseDot = () => {
    setIsOpen(false);
  };
  const ClosePopUp = (event: MouseEvent) => {
    if (Popupref.current && !Popupref.current.contains(event.target as Node)) {
      setIsOpen(false);
    }
  };

  const open = Boolean(isOpen && actions && actions?.length > 0);

  useEffect(() => {
    if (isOpen) {
      document.addEventListener("click", ClosePopUp);
    } else {
      document.removeEventListener("click", ClosePopUp);
    }

    return () => {
      document.removeEventListener("click", ClosePopUp);
    };
  }, [isOpen]);

  return (
    <div className="flex items-center justify-between" ref={Popupref}>
      <button className="bg-white rounded-full p-1 shadow" onClick={openModal}>
        <Image alt="icon" src={ASSETS_ICONS.Dote} />
      </button>
      <button onClick={CloseDot}>
        {open ? (
          <div className="absolute z-30 flex justify-center bg-white rounded-lg shadow-xl right-8 top-10 w-36 border border-neutral-2">
            <div className="grid w-36">
              {actions?.includes("detail") ? (
                <button
                  className="flex items-center space-x-2 w-full py-2 px-3 border-b border-neutral"
                  onClick={onDetail}
                >
                  <Image alt="icon" src={ASSETS_ICONS.Eyes} />
                  <p className="text-sm">Lihat</p>
                </button>
              ) : null}
              {actions?.includes("edit") ? (
                <button
                  className="flex items-center space-x-2 w-full py-2 px-3 border-b border-neutral"
                  onClick={onEdit}
                >
                  <Image alt="icon" src={ASSETS_ICONS.EditGray} />
                  <p className="text-sm">Edit</p>
                </button>
              ) : null}
              {actions?.includes("delete") ? (
                <button
                  className="flex items-center space-x-2 w-full py-2 px-3"
                  onClick={onDelete}
                >
                  <Image alt="icon" src={ASSETS_ICONS.Remove} />
                  <p className="text-danger-500 text-sm">Hapus</p>
                </button>
              ) : null}
            </div>
          </div>
        ) : null}
      </button>
    </div>
  );
};
