"use client";

import React, { useContext, useState } from "react";
import ASSETS_IMAGES from "@/configs/images";
import Image from "next/image";
import Button from "../Button";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import Spinner from "../Spinner";

type Props = {
  onConfirm: () => void;
};

export const Question: React.FC<Props> = ({ onConfirm }) => {
  const { onClose } = useContext(ModalContext);
  const [loading, setLoading] = useState(false);
  return (
    <>
      <div className="flex flex-col items-center w-full space-y-2">
        <Image alt="icon" src={ASSETS_IMAGES.Delete} />
        <h1 className="font-semibold">Konfirmasi Penghapusan</h1>
        <p className="text-center text-sm text-neutral-5">
          Apakah Anda yakin ingin menghapus <br /> secara permanen pada data
          yang dipilih?
        </p>
      </div>
      <div className="flex justify-center items-center space-x-2 mt-4">
        <Button
          className="!bg-white !border !border-primary !text-primary !px-4 !w-32"
          onClick={() => {
            setLoading(true);
            onConfirm();
          }}
        >
          {loading ? <Spinner /> : "Ya, Hapus"}
        </Button>
        <Button className="!px-4 border border-primary !w-32" onClick={onClose}>
          Batal
        </Button>
      </div>
    </>
  );
};
