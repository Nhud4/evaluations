"use client";

import React, { useContext, useState } from "react";
import ROUTES from "@/configs/router";
import Image from "next/image";
import ASSETS_IMAGES from "@/configs/images";
import ASSETS_ICONS from "@/configs/icons";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { ShowContext } from "@/context/ShowContext/ShowContext";

import styles from "./styles.module.css";

export const SideBar = () => {
  const pathname = usePathname();
  const { show } = useContext(ShowContext);
  const [openDropdown, setOpenDropdown] = useState(false);
  const dropdownPath = pathname.split("/")[1];
  const subMenu = pathname.split("/")[2];
  const [isHovering, setIsHovered] = useState(false);
  const [activeIndex, setActiveIndex] = useState("");

  return (
    <div
      className={[styles.container, show.open ? "w-64" : styles.close].join(
        " "
      )}
    >
      <div className="mb-3 pt-3 px-4">
        <div className={styles.logo}>
          <Image
            alt="logo"
            src={ASSETS_IMAGES.WhiteLogo}
            width={42}
            height={42}
          />
          <p>
            Universitas Nahdlatul <br /> Ulama Sunan Giri
          </p>
        </div>
        <div className={styles.line} />
      </div>
      <div className={styles.mainMenu}>
        <h1>MENU UTAMA</h1>
      </div>
      <ul className={styles.nav}>
        {ROUTES.map((menu, index) => {
          const active = pathname === menu.path;
          const activeDropdown = `/${dropdownPath}` === menu.path;
          const hover = isHovering && activeIndex === menu.path;
          return (
            <li key={index}>
              {menu.asDropdown ? (
                <button
                  className={[
                    styles.dropdown,
                    activeDropdown || hover ? "bg-white rounded-xl" : "",
                  ].join(" ")}
                  onClick={() => setOpenDropdown(!openDropdown)}
                  onMouseEnter={() => {
                    setIsHovered(true);
                    setActiveIndex(menu.path);
                  }}
                  onMouseLeave={() => {
                    setIsHovered(false);
                    setActiveIndex("");
                  }}
                >
                  <div className={styles.wrapperMenu}>
                    <div
                      className={[
                        "w-fit p-2 rounded-xl",
                        activeDropdown || hover ? "bg-primary" : "bg-white",
                      ].join(" ")}
                    >
                      <Image
                        alt="icons"
                        src={
                          activeDropdown || hover ? menu.activeIcon : menu.icon
                        }
                      />
                    </div>
                    <p
                      className={[
                        "font-semibold",
                        activeDropdown || hover ? "text-primary" : "text-white",
                      ].join(" ")}
                    >
                      {menu.label}
                    </p>
                  </div>
                  <Image
                    alt="icons"
                    src={
                      activeDropdown || hover
                        ? ASSETS_ICONS.ChevronGreen
                        : ASSETS_ICONS.ChevronWhite
                    }
                    className={
                      openDropdown || activeDropdown ? "rotate-90" : "rotate-0"
                    }
                  />
                </button>
              ) : (
                <Link
                  href={menu.path}
                  className={[
                    styles.menu,
                    active || hover ? styles.activeMenu : styles.inactiveMenu,
                  ].join(" ")}
                  onMouseEnter={() => {
                    setIsHovered(true);
                    setActiveIndex(menu.path);
                  }}
                  onMouseLeave={() => {
                    setIsHovered(false);
                    setActiveIndex("");
                  }}
                >
                  <div>
                    <Image
                      alt="icons"
                      src={active || hover ? menu.activeIcon : menu.icon}
                    />
                  </div>
                  <p>{menu.label}</p>
                </Link>
              )}
              {menu.children ? (
                <ul
                  className={[
                    "ml-4",
                    !openDropdown && !activeDropdown ? "hidden" : "block",
                  ].join(" ")}
                >
                  {menu.children.map((sub, key) => {
                    const active = pathname === sub.path;
                    const activeSub =
                      `/${dropdownPath}/${subMenu}` === sub.path;
                    const hover = isHovering && activeIndex === sub.path;
                    return (
                      <li key={key}>
                        <Link
                          href={sub.path}
                          className={[
                            styles.wrapperSubMenu,
                            active || activeSub || hover
                              ? "bg-white rounded-xl"
                              : "",
                          ].join(" ")}
                          onMouseEnter={() => {
                            setIsHovered(true);
                            setActiveIndex(sub.path);
                          }}
                          onMouseLeave={() => {
                            setIsHovered(false);
                            setActiveIndex("");
                          }}
                        >
                          <div
                            className={[
                              styles.dote,
                              active || activeSub || hover
                                ? "bg-primary"
                                : "bg-white",
                            ].join(" ")}
                          />
                          <p
                            className={
                              active || activeSub || hover
                                ? "text-primary"
                                : "text-white"
                            }
                          >
                            {sub.label}
                          </p>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              ) : null}
            </li>
          );
        })}
      </ul>
      <div className={styles.ornamen} />
    </div>
  );
};
