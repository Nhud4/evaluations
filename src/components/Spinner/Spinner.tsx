"use client";

import React from "react";
import ASSETS_IMAGES from "@/configs/images";
import Image from "next/image";

export const Spinner = () => {
  return (
    <div className="flex justify-center items-center">
      <div className="w-6 h-6 animate-spin">
        <Image alt="loading" src={ASSETS_IMAGES.LoadingButton} />
      </div>
    </div>
  );
};
