"use client";

import React from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

import styles from "./styles.module.css";

type Props = {
  clampLine?: number;
  loading: boolean;
  skeletonWidth?: number;
  value: string | JSX.Element;
};

export const TableCell: React.FC<Props> = ({
  clampLine,
  loading,
  skeletonWidth = 100,
  value,
}) => {
  if (loading) {
    return <Skeleton className="!z-0" width={skeletonWidth} />;
  }

  if (typeof value === "string") {
    return (
      <span
        className={styles.clampText}
        style={{ "--clamp-line": clampLine } as React.CSSProperties}
        title={value}
      >
        <p className="text-xs capitalize">{value}</p>
      </span>
    );
  }

  return value || "";
};
