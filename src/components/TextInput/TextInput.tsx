"use client";

import React, { useState } from "react";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

type Props = {
  label: string;
  placeholder: string;
  type: "text" | "password";
  labelClassName?: string;
  onChange?: (value: string | number) => void;
  value?: string;
  className?: string;
  isDisabled?: boolean;
  onlyNumber?: boolean;
  name: string;
  id?: string;
  isLoading?: boolean;
  isError?: string;
};

export const TextInput: React.FC<Props> = ({
  label,
  placeholder,
  type,
  labelClassName,
  onChange,
  value,
  className,
  isDisabled,
  onlyNumber,
  name,
  id,
  isLoading,
  isError,
}) => {
  const [showPass, setShowPass] = useState(false);
  const isPassword = type === "password";

  const transform = (value: string) => {
    if (onlyNumber) {
      return value.replace(/[^\d.]/g, "");
    }
    return value;
  };

  return (
    <div className={className}>
      <h1
        className={[
          "text-sm font-semibold",
          isLoading ? "pb-1" : "pb-2",
          labelClassName,
        ].join(" ")}
      >
        {label}
      </h1>
      {isLoading ? <Skeleton height={40} /> : null}
      {isPassword && !isLoading ? (
        <div
          className={[
            "flex items-center space-x-2 py-2 px-4 rounded-md border focus-within:border-primary",
            className,
            isDisabled ? "bg-neutral-6" : "bg-white",
            isError ? "border-danger-500" : "border-neutral-3",
          ].join(" ")}
        >
          <input
            type={showPass ? "text" : "password"}
            name={name}
            id={id}
            placeholder={placeholder}
            className="w-full text-sm"
            disabled={isDisabled}
            onChange={(e) => {
              if (onChange) {
                onChange(transform(e.target.value));
              }
            }}
            value={value}
          />
          <button
            type="button"
            onClick={() => setShowPass(!showPass)}
            className="p-[2px]"
          >
            <Image alt="Icons" src={ASSETS_ICONS.Eyes} />
          </button>
        </div>
      ) : null}
      {!isPassword && !isLoading ? (
        <div
          className={[
            "py-[0.35rem] px-4 rounded-md border border-neutral-3 focus-within:border-primary",
            className,
            isDisabled ? "bg-neutral-6" : "bg-white",
          ].join(" ")}
        >
          <input
            autoComplete="off"
            type="text"
            name={name}
            id={id}
            placeholder={placeholder}
            className="text-sm font-normal"
            disabled={isDisabled}
            onChange={(e) => {
              if (onChange) {
                onChange(transform(e.target.value));
              }
            }}
            value={value}
          />
        </div>
      ) : null}
      {isError ? (
        <div>
          <p className="text-sm text-danger-500">{isError}</p>
        </div>
      ) : null}
    </div>
  );
};
