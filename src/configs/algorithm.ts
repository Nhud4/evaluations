export const KnnProstate = (
  testing: TestingProstate[],
  data: ProstatePayload
) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.radius - parseFloat(data.radius)) ** 2 +
          (element.texture - parseFloat(data.texture)) ** 2 +
          (element.perimeter - parseFloat(data.perimeter)) ** 2 +
          (element.area - parseFloat(data.area)) ** 2 +
          (element.smoothness - parseFloat(data.smoothness)) ** 2 +
          (element.compactness - parseFloat(data.compactness)) ** 2 +
          (element.symmetry - parseFloat(data.symmetry)) ** 2 +
          (element.fractal_dimension - parseFloat(data.fractal_dimension)) ** 2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const BackwardProstate = (
  testing: TestingProstate[],
  data: ProstatePayload
) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.radius - parseFloat(data.radius)) ** 2 +
          (element.perimeter - parseFloat(data.perimeter)) ** 2 +
          (element.area - parseFloat(data.area)) ** 2 +
          (element.smoothness - parseFloat(data.smoothness)) ** 2 +
          (element.compactness - parseFloat(data.compactness)) ** 2 +
          (element.symmetry - parseFloat(data.symmetry)) ** 2 +
          (element.fractal_dimension - parseFloat(data.fractal_dimension)) ** 2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const ForwardProstate = (
  testing: TestingProstate[],
  data: ProstatePayload
) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.perimeter - parseFloat(data.perimeter)) ** 2 +
          (element.compactness - parseFloat(data.compactness)) ** 2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const KnnCancer = (testing: TestingCancer[], payload: CancerPayload) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.radius_mean - parseFloat(payload.radius_mean)) ** 2 +
          (element.texture_mean - parseFloat(payload.texture_mean)) ** 2 +
          (element.perimeter_mean - parseFloat(payload.perimeter_mean)) ** 2 +
          (element.area_mean - parseFloat(payload.area_mean)) ** 2 +
          (element.smoothness_mean - parseFloat(payload.smoothness_mean)) ** 2 +
          (element.compactness_mean - parseFloat(payload.compactness_mean)) **
            2 +
          (element.concavity_mean - parseFloat(payload.concavity_mean)) ** 2 +
          (element.concave_points_mean -
            parseFloat(payload.concave_points_mean)) **
            2 +
          (element.symmetry_mean - parseFloat(payload.symmetry_mean)) ** 2 +
          (element.fractal_dimension_mean -
            parseFloat(payload.fractal_dimension_mean)) **
            2 +
          (element.radius_se - parseFloat(payload.radius_se)) ** 2 +
          (element.texture_se - parseFloat(payload.texture_se)) ** 2 +
          (element.perimeter_se - parseFloat(payload.perimeter_se)) ** 2 +
          (element.area_se - parseFloat(payload.area_se)) ** 2 +
          (element.smoothness_se - parseFloat(payload.smoothness_se)) ** 2 +
          (element.compactness_se - parseFloat(payload.compactness_se)) ** 2 +
          (element.concavity_se - parseFloat(payload.concavity_se)) ** 2 +
          (element.concave_points_se - parseFloat(payload.concave_points_se)) **
            2 +
          (element.symmetry_se - parseFloat(payload.symmetry_se)) ** 2 +
          (element.fractal_dimension_se -
            parseFloat(payload.fractal_dimension_se)) **
            2 +
          (element.radius_worst - parseFloat(payload.radius_worst)) ** 2 +
          (element.texture_worst - parseFloat(payload.texture_worst)) ** 2 +
          (element.perimeter_worst - parseFloat(payload.perimeter_worst)) ** 2 +
          (element.area_worst - parseFloat(payload.area_worst)) ** 2 +
          (element.smoothness_worst - parseFloat(payload.smoothness_worst)) **
            2 +
          (element.compactness_worst - parseFloat(payload.compactness_worst)) **
            2 +
          (element.concavity_worst - parseFloat(payload.concavity_worst)) ** 2 +
          (element.concave_points_worst -
            parseFloat(payload.concave_points_worst)) **
            2 +
          (element.symmetry_worst - parseFloat(payload.symmetry_worst)) ** 2 +
          (element.fractal_dimension_worst -
            parseFloat(payload.fractal_dimension_worst)) **
            2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const BackwardCancer = (
  testing: TestingCancer[],
  payload: CancerPayload
) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.texture_mean - parseFloat(payload.texture_mean)) ** 2 +
          (element.perimeter_mean - parseFloat(payload.perimeter_mean)) ** 2 +
          (element.area_mean - parseFloat(payload.area_mean)) ** 2 +
          (element.smoothness_mean - parseFloat(payload.smoothness_mean)) ** 2 +
          (element.compactness_mean - parseFloat(payload.compactness_mean)) **
            2 +
          (element.concavity_mean - parseFloat(payload.concavity_mean)) ** 2 +
          (element.concave_points_mean -
            parseFloat(payload.concave_points_mean)) **
            2 +
          (element.symmetry_mean - parseFloat(payload.symmetry_mean)) ** 2 +
          (element.fractal_dimension_mean -
            parseFloat(payload.fractal_dimension_mean)) **
            2 +
          (element.radius_se - parseFloat(payload.radius_se)) ** 2 +
          (element.texture_se - parseFloat(payload.texture_se)) ** 2 +
          (element.perimeter_se - parseFloat(payload.perimeter_se)) ** 2 +
          (element.area_se - parseFloat(payload.area_se)) ** 2 +
          (element.smoothness_se - parseFloat(payload.smoothness_se)) ** 2 +
          (element.compactness_se - parseFloat(payload.compactness_se)) ** 2 +
          (element.concavity_se - parseFloat(payload.concavity_se)) ** 2 +
          (element.concave_points_se - parseFloat(payload.concave_points_se)) **
            2 +
          (element.symmetry_se - parseFloat(payload.symmetry_se)) ** 2 +
          (element.fractal_dimension_se -
            parseFloat(payload.fractal_dimension_se)) **
            2 +
          (element.radius_worst - parseFloat(payload.radius_worst)) ** 2 +
          (element.texture_worst - parseFloat(payload.texture_worst)) ** 2 +
          (element.perimeter_worst - parseFloat(payload.perimeter_worst)) ** 2 +
          (element.area_worst - parseFloat(payload.area_worst)) ** 2 +
          (element.smoothness_worst - parseFloat(payload.smoothness_worst)) **
            2 +
          (element.compactness_worst - parseFloat(payload.compactness_worst)) **
            2 +
          (element.concavity_worst - parseFloat(payload.concavity_worst)) ** 2 +
          (element.concave_points_worst -
            parseFloat(payload.concave_points_worst)) **
            2 +
          (element.symmetry_worst - parseFloat(payload.symmetry_worst)) ** 2 +
          (element.fractal_dimension_worst -
            parseFloat(payload.fractal_dimension_worst)) **
            2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const ForwardCancer = (
  testing: TestingCancer[],
  payload: CancerPayload
) => {
  try {
    const result = [];
    for (let i = 0; i < testing.length; i++) {
      const element = testing[i];
      const total = Math.sqrt(
        (element.radius_mean - parseFloat(payload.radius_mean)) ** 2 +
          (element.texture_mean - parseFloat(payload.texture_mean)) ** 2 +
          (element.perimeter_worst - parseFloat(payload.perimeter_worst)) ** 2
      );

      result.push({ id: element.id, diagnosis: element.diagnosis, total });
    }

    const euclidiance = result
      .map((item) => item)
      .sort((a, b) => a.total - b.total);

    const resultUp = euclidiance.slice(0, 5);
    let totalM = 0;
    let totalB = 0;

    for (let x = 0; x < resultUp.length; x++) {
      const element = resultUp[x];
      if (element.diagnosis === "M") {
        totalM++;
      } else {
        totalB++;
      }
    }

    const decision = totalM > totalB ? "M" : "B";
    return decision;
  } catch (error) {
    return error;
  }
};

export const CountPerformance = (data: DataPerformance[]) => {
  let isPositive = 0;
  let isNegative = 0;
  let isNotPositive = 0;
  let isNotNegative = 0;

  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    if (element.actual === "M" && element.prediction === "M") {
      isPositive += 1;
    }
    if (element.actual === "B" && element.prediction === "B") {
      isNegative += 1;
    }
    if (element.actual === "M" && element.prediction === "B") {
      isNotPositive += 1;
    }
    if (element.actual === "B" && element.prediction === "M") {
      isNotNegative += 1;
    }
  }

  const recall = isPositive / (isPositive + isNotPositive);
  const precision = isPositive / (isPositive + isNotNegative);
  const accuracy =
    (isPositive + isNegative) /
    (isPositive + isNotPositive + (isNegative + isNotNegative));
  const f1Score = 2 * ((recall * precision) / (recall + precision));

  return {
    recall,
    precision,
    accuracy: accuracy * 100,
    f1Score,
  };
};
