export const TIME_MANAGE = {
  pagi: [3, 4, 5, 6, 7, 8, 9, 10],
  siang: [11, 12, 13, 14, 15],
  sore: [16, 17, 18],
  petang: [19],
  malam: [20, 21, 22, 23, 0, 1, 2],
};

export const SUMMARY_DATA = [
  {
    label: "Total Data",
    key: "total",
    color: "#40A5A1",
  },
  {
    label: "Prostate Cancer",
    key: "prostate",
    color: "#AC4980",
  },
  {
    label: "Breast Cancer",
    key: "beast",
    color: "#127F9C",
  },
];

export const ATTR_BEAST = [
  {
    key: "knn",
    algo: "knn",
    attr: [
      "radius mean",
      "texture mean",
      "perimeter mean",
      "area mean",
      "smoothness mean",
      "compactness mean",
      "concavity mean",
      "concave points mean",
      "symmetry mean",
      "fractal dimension mean",
      "radius se",
      "texture se",
      "perimeter se",
      "area se",
      "smoothness se",
      "compactness se",
      "concavity se",
      "concave points se",
      "symmetry se",
      "fractal dimension se",
      "radius worst",
      "texture worst",
      "perimeter worst",
      "area worst",
      "smoothness worst",
      "compactness worst",
      "concavity worst",
      "concave points worst",
      "symmetry worst",
      "fractal dimension worst",
    ],
  },
  {
    key: "chi",
    algo: "chi-square",
    attr: [
      "radius mean",
      "texture mean",
      "perimeter mean",
      "area mean",
      "smoothness mean",
      "compactness mean",
      "concavity mean",
      "concave points mean",
      "symmetry mean",
      "fractal dimension mean",
      "radius se",
      "texture se",
      "perimeter se",
      "area se",
      "smoothness se",
      "compactness se",
      "concavity se",
      "concave points se",
      "symmetry se",
      "fractal dimension se",
      "radius worst",
      "texture worst",
      "perimeter worst",
      "area worst",
      "smoothness worst",
      "compactness worst",
      "concavity worst",
      "concave points worst",
      "symmetry worst",
      "fractal dimension worst",
    ],
  },
  {
    key: "backward",
    algo: "backward",
    attr: [
      "texture mean",
      "perimeter mean",
      "area mean",
      "smoothness mean",
      "compactness mean",
      "concavity mean",
      "concave points mean",
      "symmetry mean",
      "fractal dimension mean",
      "radius se",
      "texture se",
      "perimeter se",
      "area se",
      "smoothness se",
      "compactness se",
      "concavity se",
      "concave points se",
      "symmetry se",
      "fractal dimension se",
      "radius worst",
      "texture worst",
      "perimeter worst",
      "area worst",
      "smoothness worst",
      "compactness worst",
      "concavity worst",
      "concave points worst",
      "symmetry worst",
      "fractal dimension worst",
    ],
  },
  {
    key: "forward",
    algo: "forward",
    attr: ["radius mean", "texture mean", "perimeter worst"],
  },
];

export const ATTR_PROSTATE = [
  {
    key: "knn",
    algo: "knn",
    attr: [
      "radius",
      "texture",
      "perimeter",
      "area",
      "smoothness",
      "compactness",
      "symmetry",
      "fractal dimension",
    ],
  },
  {
    key: "chi",
    algo: "chi-square",
    attr: [
      "radius",
      "texture",
      "perimeter",
      "area",
      "smoothness",
      "compactness",
      "symmetry",
      "fractal dimension",
    ],
  },
  {
    key: "backward",
    algo: "backward",
    attr: [
      "radius",
      "perimeter",
      "area",
      "smoothness",
      "compactness",
      "symmetry",
      "fractal dimension",
    ],
  },
  {
    key: "forward",
    algo: "forward",
    attr: ["perimeter", "compactness"],
  },
];

export const TOTAL_ATTR = {
  prostate: {
    knn: 8,
    chi: 8,
    backward: 7,
    forward: 2,
  },
  beast: {
    knn: 30,
    chi: 30,
    backward: 29,
    forward: 3,
  },
};

export const ALGO_LIST = [
  { data: "prostate", algo: "KNN", value: "knn" },
  { data: "prostate", algo: "Chi-square", value: "chi" },
  { data: "prostate", algo: "Backward", value: "backward" },
  { data: "prostate", algo: "Forward", value: "forward" },
  { data: "beast", algo: "KNN", value: "knn" },
  { data: "beast", algo: "Chi-square", value: "chi" },
  { data: "beast", algo: "Backward", value: "backward" },
  { data: "beast", algo: "Forward", value: "forward" },
];
