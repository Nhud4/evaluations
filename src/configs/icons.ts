import ChevronBlack from "../../public/assets/icons/chevron-black.svg";
import ChevronGreen from "../../public/assets/icons/chevron-green.svg";
import ChevronWhite from "../../public/assets/icons/chevron-white.svg";
import Edit from "../../public/assets/icons/edit.svg";
import Search from "../../public/assets/icons/search.svg";
import LogOut from "../../public/assets/icons/logout.svg";
import User from "../../public/assets/icons/user-octagon.svg";
import Menu from "../../public/assets/icons/menu.svg";
import HomeWhite from "../../public/assets/icons/home-white.svg";
import HomeGreen from "../../public/assets/icons/home-green.svg";
import NoteWhite from "../../public/assets/icons/note-white.svg";
import NoteGreen from "../../public/assets/icons/note-green.svg";
import SharpWhite from "../../public/assets/icons/sharp-white.svg";
import SharpGreen from "../../public/assets/icons/sharp-green.svg";
import Document from "../../public/assets/icons/text.svg";
import Eyes from "../../public/assets/icons/eye.svg";
import Save from "../../public/assets/icons/save.svg";
import Close from "../../public/assets/icons/close.svg";
import Plus from "../../public/assets/icons/plus.svg";
import Dote from "../../public/assets/icons/dote.svg";
import EditGray from "../../public/assets/icons/edit-.svg";
import Remove from "../../public/assets/icons/close-circle.svg";

const ASSETS_ICONS = {
  ChevronBlack,
  ChevronGreen,
  ChevronWhite,
  Edit,
  Search,
  LogOut,
  User,
  Menu,
  HomeWhite,
  HomeGreen,
  NoteWhite,
  NoteGreen,
  SharpWhite,
  SharpGreen,
  Document,
  Eyes,
  Save,
  Close,
  Plus,
  Dote,
  EditGray,
  Remove,
};

export default ASSETS_ICONS;
