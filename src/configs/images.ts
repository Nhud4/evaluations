import Avatar from "../../public/assets/images/Avatar.png";
import Loading from "../../public/assets/images/loading.png";
import Empty from "../../public/assets/images/empty.png";
import WhiteLogo from "../../public/assets/images/unugiri-2.png";
import GreenLogo from "../../public/assets/images/unugiri.png";
import LoadingButton from "../../public/assets/images/traffic.png";
import Question from "../../public/assets/images/question.png";
import Delete from "../../public/assets/images/delete.png";
import Morning from "../../public/assets/images/morning.png";
import Day from "../../public/assets/images/day.png";
import Afternoon from "../../public/assets/images/afternoon.png";
import Evening from "../../public/assets/images/evening.png";
import Night from "../../public/assets/images/night.png";

const ASSETS_IMAGES = {
  Avatar,
  Loading,
  Empty,
  WhiteLogo,
  GreenLogo,
  LoadingButton,
  Question,
  Delete,
  Morning,
  Day,
  Afternoon,
  Evening,
  Night,
};

export default ASSETS_IMAGES;
