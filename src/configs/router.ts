import ASSETS_ICONS from "./icons";

const ROUTES: Routes = [
  {
    path: "/dashboard",
    label: "Dashboard",
    icon: ASSETS_ICONS.HomeGreen,
    activeIcon: ASSETS_ICONS.HomeWhite,
  },
  {
    path: "/pusat-data",
    label: "Pusat Data",
    icon: ASSETS_ICONS.NoteGreen,
    activeIcon: ASSETS_ICONS.NoteWhite,
    asDropdown: true,
    children: [
      {
        path: "/pusat-data/prostate-cancer",
        label: "Prostate Cancer",
      },
      {
        path: "/pusat-data/breast-cancer",
        label: "Breast Cancer",
      },
    ],
  },
  {
    path: "/performance",
    label: "Performance",
    icon: ASSETS_ICONS.SharpGreen,
    activeIcon: ASSETS_ICONS.SharpWhite,
  },
];

export default ROUTES;
