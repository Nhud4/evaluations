type Routes = {
  path: string;
  label: string;
  icon: string | StaticImport;
  activeIcon: string | StaticImport;
  children?: {
    path: string;
    label: string;
  }[];
  asDropdown?: boolean;
}[];

type AppleData = {
  no: number;
  code: string;
  size: string;
  weight: string;
  sweetness: string;
  crunchiness: string;
  quality: string;
};

type PaginationMeta = {
  page: number;
  totalData: number;
  totalDataOnPage?: number;
  totalPage: number;
  totalPerPage: number;
};

type Option = {
  icon?: string | React.ReactElement;
  image?: string | React.ReactElement;
  label: string | React.ReactElement;
  value: string;
};

type CancerData = {
  no: number;
  code: string;
  diagnosis: string;
  radiusMean: string;
  perimeterMean: string;
  areaMean: string;
  smoothnessMean: string;
};

type PerformanceData = {
  date: string;
  algo: string;
  dataType: string;
  accuracy: number;
  no: number;
};

type TestingProstate = {
  id: number;
  diagnosis: string;
  radius: number;
  texture: number;
  perimeter: number;
  area: number;
  smoothness: number;
  compactness: number;
  symmetry: number;
  fractal_dimension: number;
};

type TestingCancer = {
  id: number;
  diagnosis: string;
  radius_mean: number;
  texture_mean: number;
  perimeter_mean: number;
  area_mean: number;
  smoothness_mean: number;
  compactness_mean: number;
  concavity_mean: number;
  concave_points_mean: number;
  symmetry_mean: number;
  fractal_dimension_mean: number;
  radius_se: number;
  texture_se: number;
  perimeter_se: number;
  area_se: number;
  smoothness_se: number;
  compactness_se: number;
  concavity_se: number;
  concave_points_se: number;
  symmetry_se: number;
  fractal_dimension_se: number;
  radius_worst: number;
  texture_worst: number;
  perimeter_worst: number;
  area_worst: number;
  smoothness_worst: number;
  compactness_worst: number;
  concavity_worst: number;
  concave_points_worst: number;
  symmetry_worst: number;
  fractal_dimension_worst: number;
};

type DataPerformance = {
  code: string;
  actual: string;
  prediction: string;
};
