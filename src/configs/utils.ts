import { useEffect, useState } from "react";

export const formatNumber = (number: number): string => {
  return (
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    })
      .format(number)
      .replace(",00", "")
      .split("Rp")[1] || "0"
  );
};

export function useDebounce<T>(value: T, delay?: number): T {
  const [debouncedValue, setDebouncedValue] = useState<T>(value);

  useEffect(() => {
    const timer = setTimeout(() => setDebouncedValue(value), delay || 500);

    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);

  return debouncedValue;
}

export const formatQuery = (params: Record<string, unknown>) => {
  const query: string = params
    ? Object.keys(params)
        .map((key) => `${key}=${params[key]}`)
        .join("&")
    : "";
  return query;
};

export const generateNoColumn = (
  meta: PaginationMeta | undefined,
  index: number
): number => {
  if (!meta) return 1;
  const page = meta?.page as number;
  const totalPerPage = (meta?.totalPerPage || meta?.totalDataOnPage) as number;
  const no = page === 1 ? index + 1 : totalPerPage * (page - 1) + index + 1;
  return no;
};
