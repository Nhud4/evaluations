type Modal = {
  content: React.ReactNode;
  onConfirm?: () => void;
  open: boolean;
  title?: string;
};
