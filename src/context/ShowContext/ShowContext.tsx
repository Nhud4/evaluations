"use client";

import React, { createContext, useMemo, useState } from "react";

type ShowValue = {
  open: boolean;
};

type Props = {
  children: React.ReactNode;
};

export const ShowContext = createContext<{
  show: ShowValue;
  setShow: React.Dispatch<React.SetStateAction<ShowValue>>;
}>({
  show: { open: true },
  setShow: () => {},
});

export const ShowContextProvider: React.FC<Props> = ({ children }) => {
  const [show, setShow] = useState({ open: true });
  const initialValue = useMemo(() => ({ show, setShow }), [show]);

  return (
    <ShowContext.Provider value={initialValue}>{children}</ShowContext.Provider>
  );
};
