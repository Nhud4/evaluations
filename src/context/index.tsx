"use client";

import { ModalContextProvider } from "./ModalContext/ModalContext";
import { ShowContextProvider } from "./ShowContext/ShowContext";

type Props = {
  children: React.ReactNode;
};

const ContextProvider: React.FC<Props> = ({ children }) => {
  return (
    <ModalContextProvider>
      <ShowContextProvider>{children}</ShowContextProvider>
    </ModalContextProvider>
  );
};

export default ContextProvider;
