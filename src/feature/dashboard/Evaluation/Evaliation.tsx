"use client";

import React, { useEffect, useState } from "react";
import LineProcess from "@/components/LineProcess";
import { useQuery } from "@tanstack/react-query";

export const Evaluation = () => {
  const [trigger, setTrigger] = useState("0");
  const { data } = useQuery({
    queryKey: ["dashboard/table", [trigger]],
    queryFn: async () => {
      const res = await fetch("/api/dashboard/grafik", { method: "GET" });
      const { data } = await res.json();
      return data;
    },
  });

  useEffect(() => {
    setTrigger(trigger + 0);
  }, []);

  return (
    <div className="grid gap-4">
      <div className="bg-white p-4 rounded-xl drop-shadow-xl">
        <div className="space-y-1">
          <h1 className="font-semibold text-primary">Breast Cancer</h1>
          <div className="bg-primary w-20 h-[2px] rounded-full" />
        </div>
        <table className="w-full h-[85%]">
          <tbody>
            <tr>
              <td className="w-[60%] h-8 text-sm text-secondary-1">
                Algortima K-NN
              </td>
              <td>
                <LineProcess data={data?.beast?.knn} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] h-8 text-sm text-secondary-1">
                Forward Selection
              </td>
              <td>
                <LineProcess data={data?.beast?.forward} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] h-8 text-sm text-secondary-1">
                Backward Elimination
              </td>
              <td>
                <LineProcess data={data?.beast?.backward} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] h-8 text-sm text-secondary-1">
                Chi-Square
              </td>
              <td>
                <LineProcess data={data?.beast?.chi} />
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="bg-white p-4 rounded-xl drop-shadow-xl">
        <div className="space-y-1">
          <h1 className="font-semibold text-primary">Prosate Cancer</h1>
          <div className="bg-primary w-20 h-[2px] rounded-full" />
        </div>
        <table className="w-full h-[85%]">
          <tbody>
            <tr>
              <td className="w-[60%] text-sm text-secondary-1">
                Algortima K-NN
              </td>
              <td>
                <LineProcess data={data?.prostate?.knn} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] text-sm text-secondary-1">
                Forward Selection
              </td>
              <td>
                <LineProcess data={data?.prostate?.forward} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] text-sm text-secondary-1">
                Backward Elimination
              </td>
              <td>
                <LineProcess data={data?.prostate?.backward} />
              </td>
            </tr>
            <tr>
              <td className="w-[60%] text-sm text-secondary-1">Chi-Square</td>
              <td>
                <LineProcess data={data?.prostate?.chi} />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};
