"use client";

import React, { useEffect, useState } from "react";
import { SUMMARY_DATA } from "@/configs/dummy";
import Image from "next/image";
import ASSETS_ICONS from "@/configs/icons";
import { formatNumber } from "@/configs/utils";
import { useQuery } from "@tanstack/react-query";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export const Summary = () => {
  const [trigger, setTrigger] = useState("0");
  const { data, isLoading } = useQuery({
    queryKey: ["dashboard/summary", [trigger]],
    queryFn: async () => {
      const res = await fetch("/api/dashboard/summary", { method: "GET" });
      const { data } = await res.json();
      return data;
    },
  });

  useEffect(() => {
    setTrigger(trigger + 0);
  }, []);

  return (
    <div className="grid grid-cols-3 gap-4">
      {SUMMARY_DATA.map((item, index) => (
        <div
          key={index}
          className="flex justify-between items-center bg-white py-2 px-4 rounded-xl drop-shadow-xl"
        >
          <div>
            <h1 className="text-sm text-secondary-1">{item.label}</h1>
            {isLoading ? (
              <Skeleton />
            ) : (
              <h2 className="font-semibold -ml-1">
                {formatNumber(data[item.key])}
              </h2>
            )}
          </div>
          <div
            className="p-2 rounded-xl"
            style={{ backgroundColor: item.color }}
          >
            <Image alt="icons" src={ASSETS_ICONS.Document} />
          </div>
        </div>
      ))}
    </div>
  );
};
