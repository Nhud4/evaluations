"use client";

import React, { useEffect, useState } from "react";
import TextInput from "@/components/TextInput";
import Button from "@/components/Button";
import Image from "next/image";
import ASSETS_IMAGES from "@/configs/images";
import Link from "next/link";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import Spinner from "@/components/Spinner";

import styles from "./styles.module.css";

const initialValue = {
  username: "",
  password: "",
};

export const FormLogin = () => {
  const [formValue, setFormValue] = useState(initialValue);
  const router = useRouter();

  const { mutate, isPending } = useMutation({
    mutationFn: (body: typeof formValue) => {
      return fetch("/api/auth/login", {
        method: "POST",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        router.push("/dashboard");
        toast.success("Berhasil masuk");
      } else {
        toast.error("Username atau password salah");
      }
    },
    onError: () => {
      toast.error("Username atau password salah");
    },
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    mutate(formValue);
  };

  const validValue = Boolean(formValue.username && formValue.password);

  return (
    <>
      <div className="absolute top-0 p-4">
        <div className="flex items-center space-x-2">
          <Image
            alt="logo"
            src={ASSETS_IMAGES.GreenLogo}
            width={52}
            height={52}
          />
          <p className="font-bold">
            Universitas Nahdlatul <br /> Ulama Sunan Giri
          </p>
        </div>
      </div>
      <div className="flex flex-col justify-center items-center w-[55%] h-screen">
        <div className="w-[20rem]">
          <h1 className="text-2xl font-bold text-primary">Masuk</h1>
          <p className="text-sm text-secondary-1">
            Masukkan username dan password untuk dapat melanjutkan
          </p>
          <form className="space-y-3 pt-4" onSubmit={onSubmit}>
            <TextInput
              label="Username"
              name="Username"
              placeholder="username"
              type="text"
              value={formValue.username}
              onChange={(value) =>
                setFormValue({ ...formValue, username: value as string })
              }
            />
            <TextInput
              label="Password"
              name="Username"
              placeholder="password"
              type="password"
              value={formValue.password}
              onChange={(value) =>
                setFormValue({ ...formValue, password: value as string })
              }
            />
            <div className="pt-2">
              <Button type="submit" isDisabled={!validValue}>
                {isPending ? <Spinner /> : "Masuk"}
              </Button>
            </div>
          </form>
        </div>
        <div className="pt-4 text-sm text-secondary-1">
          Belum punya kaun?
          <Link
            className="pl-1 text-primary font-semibold text-sm"
            href={"/register"}
          >
            Registrasi
          </Link>
        </div>
      </div>
      <div className={styles.info}>
        <h1 className="text-3xl font-bold text-white">Selamat Datang</h1>
        <p className="text-center text-white">
          Di aplikasi perbandingan akurasi antara <br /> algoritma K-NN,
          algoritma K-NN dengan seleksi fitur forward selection, <br />{" "}
          Algoritma K-NN dengan seleksi fitur backward elimination dan <br />{" "}
          Algoritma K-NN dengan seleksi fitur chi-square
        </p>
      </div>
    </>
  );
};
