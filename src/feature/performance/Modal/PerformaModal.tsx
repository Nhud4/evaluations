"use client";

import React from "react";

type Props = {
  data: string[];
  algo: string;
  dataType: string;
};

export const PerformanceModal: React.FC<Props> = ({ data, algo, dataType }) => {
  const isFirst = data.filter((item, index) => index < 10);
  const isSecond = data.filter((item, index) => index >= 10 && index < 20);
  const isThird = data.filter((item, index) => index >= 20);
  return (
    <div>
      <h2 className="text-sm font-semibold">
        Daftar atribut data {dataType} algoritma {algo}:
      </h2>
      <div className="grid grid-cols-3 gap-4 mt-4">
        <ul className="space-y-2">
          {isFirst.map((item, index) => {
            return (
              <li className="text-sm capitalize" key={index}>
                {index + 1}. {item}
              </li>
            );
          })}
        </ul>
        <ul className="space-y-2">
          {isSecond.map((item, index) => {
            return (
              <li className="text-sm capitalize" key={index}>
                {index + 11}. {item}
              </li>
            );
          })}
        </ul>
        <ul className="space-y-2">
          {isThird.map((item, index) => {
            return (
              <li className="text-sm capitalize" key={index}>
                {index + 21}. {item}
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
