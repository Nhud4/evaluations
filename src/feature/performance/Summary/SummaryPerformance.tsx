"use client";

import React, { useContext, useMemo } from "react";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import {
  TOTAL_ATTR,
  ATTR_BEAST,
  ATTR_PROSTATE,
  ALGO_LIST,
} from "@/configs/dummy";
import PerformanceModal from "../Modal";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

type Props = {
  dataType: string;
  isLoading?: boolean;
};

export const SummaryPerformance: React.FC<Props> = ({
  dataType,
  isLoading,
}) => {
  const { setModal } = useContext(ModalContext);
  const data = useMemo(() => {
    const values = ALGO_LIST.filter((item) => item.data === dataType);
    return values;
  }, [dataType]);

  const handleModal = (data: string, algo: string, keys: string) => {
    const values = data === "prostate" ? ATTR_PROSTATE : ATTR_BEAST;
    const dataValues = values.filter((item) => item.key === keys);
    setModal({
      open: true,
      content: (
        <PerformanceModal
          data={dataValues[0].attr}
          algo={algo}
          dataType={data}
        />
      ),
      title: "Daftar Atribut",
    });
  };

  return (
    <div className="grid grid-cols-4 gap-3 mt-3">
      {data.map((item, key) => (
        <div
          key={key}
          className="flex justify-between items-center p-4 bg-white rounded-lg drop-shadow-md cursor-pointer"
          onClick={() => {
            handleModal(item.data, item.algo, item.value);
          }}
        >
          <div>
            <h1 className="text-sm text-neutral-5 pb-2">
              Total Atribut {item.algo}
            </h1>
            {isLoading ? (
              <Skeleton />
            ) : (
              <h2 className="font-semibold">
                {
                  TOTAL_ATTR[item.data as keyof typeof TOTAL_ATTR][
                    item.value as "knn" | "chi" | "backward" | "forward"
                  ]
                }
              </h2>
            )}
          </div>
          <Image
            alt="icons"
            src={ASSETS_ICONS.ChevronBlack}
            className="-rotate-90"
          />
        </div>
      ))}
    </div>
  );
};
