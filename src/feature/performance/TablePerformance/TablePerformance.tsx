"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/BaseTable";
import Button from "@/components/Button";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import FormPerformance from "@/form/FormPerformance";
import Dropdown from "@/components/Dropdown";
import { useQuery } from "@tanstack/react-query";
import { formatQuery } from "@/configs/utils";
import SummaryPerformance from "../Summary";

import { columns } from "./columns";

const options = [
  { label: "Beast cancer", value: "beast" },
  { label: "Prostate cancer", value: "prostate" },
];

const initialParams = {
  dataType: "beast",
};

export const TablePerformance = () => {
  const { setModal, onClose } = useContext(ModalContext);
  const [params, setParams] = useState(initialParams);
  const [trigger, setTrigger] = useState("0");

  const handleSuccess = () => {
    onClose();
    setTrigger(trigger + 0);
  };

  const { data, isLoading } = useQuery({
    queryKey: ["performance/list", [params, trigger]],
    queryFn: async () => {
      const res = await fetch(`/api/performance/list?${formatQuery(params)}`, {
        method: "GET",
      });

      const { data } = await res.json();
      return {
        data: data?.map((item: any, index: number) => ({
          ...item,
          date: item.created_at,
          dataType:
            item.data_type === "beast" ? "Beast Cancer" : "Prostate Cancer",
          no: index + 1,
        })),
      };
    },
  });

  const onCount = () => {
    setModal({
      open: true,
      content: <FormPerformance handleSuccess={handleSuccess} />,
      title: "Hitung Akurasi Data",
    });
  };

  return (
    <BaseTable
      headerClassName="!justify-end"
      actionComponent={
        <div className="flex items-center space-x-2">
          <Dropdown
            isClearable={false}
            name="dataType"
            options={options}
            defaultValue={options[0]}
            onChange={(ops) => {
              setParams({
                ...params,
                dataType: ops === null ? "beast" : (ops.value as string),
              });
            }}
          />
          <Button onClick={onCount} className="w-40">
            Hitung Akurasi
          </Button>
        </div>
      }
      summary={
        <SummaryPerformance dataType={params.dataType} isLoading={isLoading} />
      }
      columns={columns(isLoading)}
      data={data?.data}
      isLoading={isLoading}
      title="Data Performance"
    />
  );
};
