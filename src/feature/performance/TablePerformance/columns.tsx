"use client";

import React from "react";
import TableCell from "@/components/TableCell";
import { TableColumn } from "react-data-table-component";
import LineProcess from "@/components/LineProcess";
import { format, isValid } from "date-fns";
import { id } from "date-fns/locale";

export const columns = (loading: boolean): TableColumn<PerformanceData>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={25} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "Tanggal Hitung",
    cell: ({ date }) => (
      <TableCell
        loading={loading}
        value={
          isValid(new Date(date))
            ? `${format(new Date(date), "dd-MM-yyyy HH:mm", {
                locale: id,
              })} WIB`
            : `${format(new Date(), "dd-MM-yyyy HH:mm", {
                locale: id,
              })} WIB`
        }
      />
    ),
  },
  {
    name: "Algoritma",
    cell: ({ algo }) => <TableCell loading={loading} value={algo} />,
  },
  {
    name: "Jenis Data",
    cell: ({ dataType }) => <TableCell loading={loading} value={dataType} />,
  },
  {
    name: "Akurasi",
    cell: ({ accuracy }) => (
      <TableCell
        loading={loading}
        value={
          <div className="w-full">
            <LineProcess data={accuracy} />
          </div>
        }
      />
    ),
  },
];
