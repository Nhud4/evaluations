"use client";

import React from "react";
import TableCell from "@/components/TableCell";
import { TableColumn } from "react-data-table-component";
import PopUp from "@/components/PopUp";

type Props = {
  loading: boolean;
  onEdit: (data: Prostate) => void;
  onDetail: (data: Prostate) => void;
  onDelete: (code: string) => void;
};

export const columns = ({
  loading,
  onEdit,
  onDetail,
  onDelete,
}: Props): TableColumn<Prostate>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={25} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "ID",
    cell: ({ code }) => <TableCell loading={loading} value={code} />,
  },
  {
    name: "Diagnosis",
    cell: ({ diagnosis }) => (
      <TableCell
        loading={loading}
        value={diagnosis === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "KNN",
    cell: ({ knn }) => (
      <TableCell
        loading={loading}
        value={knn === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Chi Square",
    cell: ({ chi_square }) => (
      <TableCell
        loading={loading}
        value={chi_square === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Forward",
    cell: ({ forward }) => (
      <TableCell
        loading={loading}
        value={forward === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Backward",
    cell: ({
      no,
      id,
      code,
      diagnosis,
      radius,
      texture,
      perimeter,
      area,
      smoothness,
      compactness,
      symmetry,
      fractal_dimension,
      chi_square,
      forward,
      backward,
      knn,
    }) => (
      <TableCell
        loading={loading}
        value={
          <div className="flex justify-between items-center w-full">
            <p>
              {backward === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
            </p>
            <PopUp
              actions={["detail", "edit", "delete"]}
              onDelete={() => onDelete(code)}
              onEdit={() =>
                onEdit({
                  no,
                  id,
                  code,
                  diagnosis,
                  radius,
                  texture,
                  perimeter,
                  area,
                  smoothness,
                  compactness,
                  symmetry,
                  fractal_dimension,
                  chi_square,
                  forward,
                  backward,
                  knn,
                })
              }
              onDetail={() =>
                onDetail({
                  no,
                  id,
                  code,
                  diagnosis,
                  radius,
                  texture,
                  perimeter,
                  area,
                  smoothness,
                  compactness,
                  symmetry,
                  fractal_dimension,
                  chi_square,
                  forward,
                  backward,
                  knn,
                })
              }
            />
          </div>
        }
      />
    ),
  },
];
