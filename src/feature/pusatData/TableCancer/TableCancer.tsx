"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/BaseTable";
import Button from "@/components/Button";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";
import Link from "next/link";
import { useQuery, useMutation } from "@tanstack/react-query";
import { formatQuery } from "@/configs/utils";
import { generateNoColumn } from "@/configs/utils";
import { useRouter } from "next/navigation";
import Question from "@/components/Question";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import { toast } from "react-toastify";

import { columns } from "./columns";

const initialParams: TableParams = {
  page: 1,
  size: 10,
};

export const CancerTable = () => {
  const [params, setParams] = useState(initialParams);
  const [trigger, setTrigger] = useState("0");
  const router = useRouter();
  const { setModal, onClose } = useContext(ModalContext);

  const { data, isLoading } = useQuery({
    queryKey: ["cancer/list", [params, trigger]],
    queryFn: async () => {
      const res = await fetch(`/api/data/list/cancer?${formatQuery(params)}`, {
        method: "GET",
      });

      const { data, meta } = await res.json();
      return {
        data: data?.map((item: any, index: number) => ({
          ...item,
          no: generateNoColumn(meta, index),
        })),
        meta,
      };
    },
  });

  const handleSuccess = () => {
    onClose();
    setTrigger(trigger + 0);
    setParams({ page: 1, size: 10 });
  };

  const { mutate } = useMutation({
    mutationKey: ["cancer/delete"],
    mutationFn: (code: string) => {
      return fetch(`/api/data/delete/cancer/${code}`, { method: "DELETE" });
    },
    onSuccess: (data) => {
      if (data.ok) {
        handleSuccess();
        toast.success("Berhasil menghapus data cancer");
      } else {
        toast.error("Gagal menghapus data cancer");
      }
    },
    onError: () => {
      toast.error("Gagal menghapus data cancer");
    },
  });

  const onDelete = (code: string) => {
    setModal({
      open: true,
      content: (
        <Question
          onConfirm={() => {
            mutate(code);
          }}
        />
      ),
    });
  };

  const onSearch = (search: string) => {
    if (search !== null) {
      setParams({ ...params, search, page: 1 });
    }
  };

  const onChangePage = (page: number) => {
    setParams({ ...params, page });
  };

  const onChangeRowPerPage = (page: number, size: number) => {
    setParams({ ...params, page: 1, size });
  };

  return (
    <BaseTable
      actionComponent={
        <Link href={"/pusat-data/breast-cancer/add"}>
          <Button>
            <div className="flex items-center space-x-2">
              <Image alt="icon" src={ASSETS_ICONS.Plus} />
              <p>Tambah data</p>
            </div>
          </Button>
        </Link>
      }
      columns={columns({
        loading: isLoading,
        onEdit: (code) => {
          router.push(`/pusat-data/breast-cancer/edit/${code}`);
        },
        onDetail: (code) => {
          router.push(`/pusat-data/breast-cancer/detail/${code}`);
        },
        onDelete: (code) => onDelete(code),
      })}
      data={data?.data}
      meta={data?.meta}
      isLoading={isLoading}
      onSearch={onSearch}
      onChangePage={onChangePage}
      onChangeRowPerPage={onChangeRowPerPage}
      title="Data Beast Cancer"
    />
  );
};
