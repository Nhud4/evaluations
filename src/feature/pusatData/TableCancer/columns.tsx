"use client";

import React from "react";
import TableCell from "@/components/TableCell";
import { TableColumn } from "react-data-table-component";
import PopUp from "@/components/PopUp";

type Props = {
  loading: boolean;
  onEdit: (code: string) => void;
  onDelete: (code: string) => void;
  onDetail: (code: string) => void;
};

export const columns = ({
  loading,
  onEdit,
  onDelete,
  onDetail,
}: Props): TableColumn<CancerDataList>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={25} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "ID",
    cell: ({ code }) => <TableCell loading={loading} value={code} />,
  },
  {
    name: "Diagnosis",
    cell: ({ diagnosis }) => (
      <TableCell
        loading={loading}
        value={diagnosis === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "KNN",
    cell: ({ knn }) => (
      <TableCell
        loading={loading}
        value={knn === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Chi Square",
    cell: ({ chi_square }) => (
      <TableCell
        loading={loading}
        value={chi_square === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Forward",
    cell: ({ forward }) => (
      <TableCell
        loading={loading}
        value={forward === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
      />
    ),
  },
  {
    name: "Backward",
    cell: ({ backward, code }) => (
      <TableCell
        loading={loading}
        value={
          <div className="flex justify-between items-center w-full">
            <p>
              {backward === "M" ? "Kangker Ganas (M)" : "Kangker Jinak (B)"}
            </p>
            <PopUp
              actions={["detail", "edit", "delete"]}
              onEdit={() => onEdit(code)}
              onDelete={() => onDelete(code)}
              onDetail={() => onDetail(code)}
            />
          </div>
        }
      />
    ),
  },
];
