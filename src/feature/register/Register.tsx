"use client";

import React, { useState } from "react";
import TextInput from "@/components/TextInput";
import Button from "@/components/Button";
import Link from "next/link";
import Image from "next/image";
import ASSETS_IMAGES from "@/configs/images";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

import styles from "./styles.module.css";

const initialValue = {
  name: "",
  email: "",
  username: "",
  password: "",
};

export const FormRegister = () => {
  const [formValue, setFormValue] = useState(initialValue);
  const router = useRouter();

  const { mutate, isPending } = useMutation({
    mutationFn: (body: typeof initialValue) => {
      return fetch("api/auth/register", {
        method: "POST",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        toast.error("Berhasil registreasi");
        router.push("/login");
      } else {
        toast.error("Username sudah data");
      }
    },
    onError: () => {
      toast.error("Username sudah data");
    },
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    mutate(formValue);
  };

  const validValue = Boolean(
    formValue.name &&
      formValue.email &&
      formValue.username &&
      formValue.password
  );

  return (
    <>
      <div className="absolute top-0 w-full p-4">
        <div className={styles.info}>
          <div className="flex items-center space-x-2">
            <Image
              alt="logo"
              src={ASSETS_IMAGES.GreenLogo}
              width={52}
              height={52}
            />
            <p className="font-bold text-white">
              Universitas Nahdlatul <br /> Ulama Sunan Giri
            </p>
          </div>
        </div>
      </div>
      <div className="grid place-items-center min-h-screen">
        <div className="z-50">
          <h1 className="text-center text-2xl font-bold text-white">
            Halaman Pendaftaran
          </h1>
          <p className="text-center text-sm text-white">
            Silahkan isi formulir pendaftaran untuk <br /> dapat membuat akun
            baru anda
          </p>
          <div className="flex flex-col items-center bg-white p-6 rounded-xl drop-shadow-xl mt-8">
            <form className="w-[20rem] space-y-3" onSubmit={onSubmit}>
              <h1 className="text-center text-xl font-bold">Daftar</h1>
              <TextInput
                name="name"
                label="Nama"
                placeholder="Nama"
                type="text"
                value={formValue.name}
                onChange={(value) =>
                  setFormValue({ ...formValue, name: value as string })
                }
              />
              <TextInput
                name="email"
                label="Email"
                placeholder="email@email.com"
                type="text"
                value={formValue.email}
                onChange={(value) =>
                  setFormValue({ ...formValue, email: value as string })
                }
              />
              <TextInput
                name="username"
                label="Username"
                placeholder="Username"
                type="text"
                value={formValue.username}
                onChange={(value) =>
                  setFormValue({ ...formValue, username: value as string })
                }
              />
              <TextInput
                name="password"
                label="Password"
                placeholder="Password"
                type="password"
                value={formValue.password}
                onChange={(value) =>
                  setFormValue({ ...formValue, password: value as string })
                }
              />
              <div className="pt-6">
                <Button type="submit" isDisabled={!validValue}>
                  {isPending ? (
                    <div className="flex justify-center items-center">
                      <div className="w-8 h-8 animate-spin">
                        <Image
                          alt="loading"
                          src={ASSETS_IMAGES.LoadingButton}
                        />
                      </div>
                    </div>
                  ) : (
                    "Masuk"
                  )}
                </Button>
              </div>
            </form>
            <div className="pt-4 text-sm text-secondary-1">
              Sudah punya akun?
              <Link
                className="pl-1 text-sm font-semibold text-primary"
                href={"/login"}
              >
                Masuk
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
