"use client";

import React, { useEffect, useState } from "react";
import TextInput from "@/components/TextInput";
import Button from "@/components/Button";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";
import { useQuery, useMutation } from "@tanstack/react-query";
import { toast } from "react-toastify";
import Spinner from "@/components/Spinner";

import styles from "./styles.module.css";

const initialValue = {
  name: "",
  email: "",
  username: "",
  password: "",
  confirmPassword: "",
};

type Props = {
  handleSuccess: () => void;
};

export const FormSetting = ({ handleSuccess }: Props) => {
  const [isEdit, setIsEdit] = useState(false);
  const [formValues, setFormValues] = useState(initialValue);

  const { data, isLoading } = useQuery({
    queryKey: ["auth/profile"],
    queryFn: async () => {
      const res = await fetch("/api/auth/profile", { method: "GET" });
      return await res.json();
    },
  });

  useEffect(() => {
    if (data?.data || !isEdit) {
      setFormValues({
        ...initialValue,
        name: data?.data?.name,
        email: data?.data?.email,
        username: data?.data?.username,
        password: "",
      });
    }
  }, [data, isEdit]);

  const { mutate, isPending } = useMutation({
    mutationKey: ["profile/update"],
    mutationFn: (body: AuthPayload) => {
      return fetch(`/api/auth/update/${data?.data?.id}`, {
        method: "PUT",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        handleSuccess();
        toast.success("Berhasil merubah data profile");
      } else {
        toast.error("Gagal merubah data profile");
      }
    },
    onError: () => {
      toast.error("Gagal merubah data profile");
    },
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const payload = { ...formValues };
    if (formValues.password) {
      payload.password = formValues.password;
    }

    mutate(payload);
  };

  const notValidConfirm = Boolean(
    formValues.confirmPassword &&
      formValues.password !== formValues.confirmPassword
  );

  return (
    <form className="space-y-2 w-[600px]" onSubmit={onSubmit}>
      <div className={styles.info}>
        <div className="flex justify-between items-center">
          <input
            type="text"
            style={{ background: "transparent" }}
            className={[
              "text-xl font-semibold text-white w-[300px] capitalize",
              isEdit ? "border-b border-white" : "",
            ].join(" ")}
            value={formValues.name}
            disabled={!isEdit}
            onChange={(e) => {
              setFormValues({
                ...formValues,
                name: e.target.value,
              });
            }}
          />
          {!isEdit ? (
            <button
              type="button"
              className="flex items-center space-x-2 border-b border-white"
              onClick={() => setIsEdit(!isEdit)}
            >
              <Image alt="icons" src={ASSETS_ICONS.Edit} />
              <p className="text-sm text-white">Edit</p>
            </button>
          ) : null}
        </div>
        <p className="text-sm text-white">Administrator</p>
      </div>
      <div className="grid grid-cols-2 gap-4">
        <TextInput
          name="email"
          label="Email"
          placeholder="email@email.com"
          type="text"
          isDisabled={!isEdit}
          value={formValues.email}
          isLoading={isLoading}
          onChange={(value) =>
            setFormValues({ ...formValues, email: value as string })
          }
        />
        <TextInput
          name="username"
          label="Username"
          placeholder="Username"
          type="text"
          isDisabled={!isEdit}
          value={formValues.username}
          isLoading={isLoading}
          onChange={(value) =>
            setFormValues({ ...formValues, username: value as string })
          }
        />
        <TextInput
          name="password"
          label="Password"
          placeholder="Password"
          type="password"
          isDisabled={!isEdit}
          value={formValues.password}
          isLoading={isLoading}
          onChange={(value) =>
            setFormValues({ ...formValues, password: value as string })
          }
        />
        <TextInput
          name="confirmPassword"
          label="Konfirmasi Password"
          placeholder="Konfirmasi Password"
          type="password"
          isDisabled={!isEdit}
          value={formValues.confirmPassword}
          isLoading={isLoading}
          onChange={(value) =>
            setFormValues({ ...formValues, confirmPassword: value as string })
          }
          isError={notValidConfirm ? "Konfirmasi password tidak sama" : ""}
        />
      </div>
      {isEdit ? (
        <div className="flex justify-end items-center space-x-2 pt-4">
          <Button
            className="bg-white !border !border-primary !text-primary !px-4 !w-fit"
            onClick={() => {
              setIsEdit(false);
            }}
          >
            Batal
          </Button>
          <Button className="!px-4 border border-primary !w-fit" type="submit">
            {isPending ? <Spinner /> : "Konfirmasi"}
          </Button>
        </div>
      ) : null}
    </form>
  );
};
