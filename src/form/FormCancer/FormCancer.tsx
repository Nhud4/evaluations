"use client";

import React, { useEffect, useState } from "react";
import Button from "@/components/Button";
import ASSETS_ICONS from "@/configs/icons";
import Image from "next/image";
import FormCancerOne from "./FormCancerOne";
import FormCancerTwo from "./FormCancerTwo";
import FormCancerThree from "./FormCancerThree";
import { useMutation, useQuery } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useRouter, useParams } from "next/navigation";
import Spinner from "@/components/Spinner";

type Props = {
  formType: "add" | "edit";
};

const defaultValue: CancerFormPayload = {
  diagnosis: "",
  radius_mean: "",
  texture_mean: "",
  perimeter_mean: "",
  area_mean: "",
  smoothness_mean: "",
  compactness_mean: "",
  concavity_mean: "",
  concave_points_mean: "",
  symmetry_mean: "",
  fractal_dimension_mean: "",
  radius_se: "",
  texture_se: "",
  perimeter_se: "",
  area_se: "",
  smoothness_se: "",
  compactness_se: "",
  concavity_se: "",
  concave_points_se: "",
  symmetry_se: "",
  fractal_dimension_se: "",
  radius_worst: "",
  texture_worst: "",
  perimeter_worst: "",
  area_worst: "",
  smoothness_worst: "",
  compactness_worst: "",
  concavity_worst: "",
  concave_points_worst: "",
  symmetry_worst: "",
  fractal_dimension_worst: "",
};

export const FormCancer: React.FC<Props> = ({ formType }) => {
  const [showForm, setShowForm] = useState(1);
  const [formValues, setFormValues] = useState(defaultValue);
  const router = useRouter();
  const params = useParams()["slug"];

  const { data, isLoading } = useQuery({
    queryKey: ["/cancer/detail", [params]],
    queryFn: async () => {
      const res = await fetch(`/api/data/detail/cancer/${params}`, {
        method: "GET",
      });
      const responseData = await res.json();
      return responseData;
    },
  });

  useEffect(() => {
    if (data?.data) {
      setFormValues({ ...formValues, ...data.data });
    }
  }, [data]);

  const { mutate: AddMutate, isPending } = useMutation({
    mutationKey: ["cancer/add"],
    mutationFn: (body: CancerFormPayload) => {
      return fetch("/api/data/create/cancer", {
        method: "POST",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        toast.success("Berhasil menambahkan data apple");
        router.push("/pusat-data/breast-cancer");
      } else {
        toast.error("Gagal menambahkan data apple");
      }
    },
    onError: () => {
      toast.error("Gagal menambahkan data apple");
    },
  });

  const { mutate: EditMutate, isPending: EditLoading } = useMutation({
    mutationKey: ["cancer/edit"],
    mutationFn: (body: CancerFormPayload) => {
      return fetch(`/api/data/update/cancer/${params}`, {
        method: "PUT",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        toast.success("Berhasil merubah data apple");
        router.push("/pusat-data/breast-cancer");
      } else {
        toast.error("Gagal merubah data apple");
      }
    },
    onError: () => {
      toast.error("Gagal merubah data apple");
    },
  });

  const onSubmit = () => {
    if (formType === "add") {
      AddMutate(formValues);
    } else {
      EditMutate(formValues);
    }
  };

  const checkFormOne = Boolean(
    formValues.diagnosis &&
      formValues.radius_mean &&
      formValues.texture_mean &&
      formValues.perimeter_mean &&
      formValues.area_mean &&
      formValues.smoothness_mean &&
      formValues.compactness_mean &&
      formValues.concavity_mean &&
      formValues.concave_points_mean &&
      formValues.symmetry_mean &&
      formValues.fractal_dimension_mean
  );

  const checkFormTwo = Boolean(
    formValues.radius_se &&
      formValues.texture_se &&
      formValues.perimeter_se &&
      formValues.area_se &&
      formValues.smoothness_se &&
      formValues.compactness_se &&
      formValues.concavity_se &&
      formValues.concave_points_se &&
      formValues.symmetry_se &&
      formValues.fractal_dimension_se
  );

  const checkFormThree = Boolean(
    formValues.radius_worst &&
      formValues.texture_worst &&
      formValues.perimeter_worst &&
      formValues.area_worst &&
      formValues.smoothness_worst &&
      formValues.compactness_worst &&
      formValues.concavity_worst &&
      formValues.concave_points_worst &&
      formValues.symmetry_worst &&
      formValues.fractal_dimension_worst
  );
  return (
    <div>
      <div className="flex items-center pb-4">
        <div className="flex justify-center items-center w-8 h-8 bg-primary rounded-full">
          <h1 className="text-white">1</h1>
        </div>
        <div
          className={[
            "w-14 h-1",
            showForm >= 2 ? "bg-primary" : "bg-neutral-5",
          ].join(" ")}
        />
        <div
          className={[
            "flex justify-center items-center w-8 h-8 rounded-full",
            showForm >= 2 ? "bg-primary" : "bg-neutral-5",
          ].join(" ")}
        >
          <h1 className="text-white">2</h1>
        </div>
        <div
          className={[
            "w-14 h-1",
            showForm === 3 ? "bg-primary" : "bg-neutral-5",
          ].join(" ")}
        />
        <div
          className={[
            "flex justify-center items-center w-8 h-8 rounded-full",
            showForm === 3 ? "bg-primary" : "bg-neutral-5",
          ].join(" ")}
        >
          <h1 className="text-white">3</h1>
        </div>
      </div>
      {showForm === 1 ? (
        <FormCancerOne
          formValues={formValues}
          setFormValues={(value) => setFormValues({ ...formValues, ...value })}
          isEdit={formType === "edit"}
          isLoading={isLoading}
        />
      ) : null}
      {showForm === 2 ? (
        <FormCancerTwo
          formValues={formValues}
          setFormValues={(value) => setFormValues({ ...formValues, ...value })}
          isLoading={isLoading}
        />
      ) : null}
      {showForm === 3 ? (
        <FormCancerThree
          formValues={formValues}
          setFormValues={(value) => setFormValues({ ...formValues, ...value })}
          isLoading={isLoading}
        />
      ) : null}
      {showForm === 1 ? (
        <div className="flex justify-end pt-4 w-full">
          <Button
            className="!w-fit"
            onClick={() => setShowForm(showForm + 1)}
            isDisabled={isLoading || !checkFormOne}
          >
            <div className="flex items-center space-x-2">
              <p>Halaman Selanjutnya</p>
              <Image alt="icon" src={ASSETS_ICONS.ChevronWhite} />
            </div>
          </Button>
        </div>
      ) : null}
      {showForm === 2 ? (
        <div className="flex justify-between items-center pt-4">
          <Button
            className="!w-fit"
            onClick={() => setShowForm(showForm - 1)}
            isDisabled={isLoading}
          >
            <div className="flex items-center space-x-2">
              <Image
                alt="icon"
                src={ASSETS_ICONS.ChevronWhite}
                className="rotate-180"
              />
              <p>Halaman Sebelumnya</p>
            </div>
          </Button>
          <Button
            className="!w-fit"
            onClick={() => setShowForm(showForm + 1)}
            isDisabled={isLoading || !checkFormTwo}
          >
            <div className="flex items-center space-x-2">
              <p>Halaman Selanjutnya</p>
              <Image alt="icon" src={ASSETS_ICONS.ChevronWhite} />
            </div>
          </Button>
        </div>
      ) : null}
      {showForm === 3 ? (
        <div className="flex justify-between items-center pt-4">
          <Button
            className="!w-fit"
            onClick={() => setShowForm(showForm - 1)}
            isDisabled={isLoading}
          >
            <div className="flex items-center space-x-2">
              <Image
                alt="icon"
                src={ASSETS_ICONS.ChevronWhite}
                className="rotate-180"
              />
              <p>Halaman Sebelumnya</p>
            </div>
          </Button>
          <Button
            className="!w-fit"
            onClick={onSubmit}
            isDisabled={isLoading || !checkFormThree}
          >
            <div className="flex items-center space-x-2">
              {isPending || EditLoading ? (
                <Spinner />
              ) : (
                <Image alt="icon" src={ASSETS_ICONS.Save} />
              )}
              <p>Simpan Perubahan</p>
            </div>
          </Button>
        </div>
      ) : null}
    </div>
  );
};
