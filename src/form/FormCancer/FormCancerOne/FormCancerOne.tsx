"use client";

import React from "react";
import TextInput from "@/components/TextInput";
import Dropdown from "@/components/Dropdown";

type Props = {
  formValues: CancerFormPayload;
  setFormValues: (value: CancerFormPayload) => void;
  isEdit?: boolean;
  isLoading?: boolean;
};

const options = [
  { label: "Kangker Ganas (M)", value: "M" },
  { label: "Kangker Jinak (B)", value: "B" },
];

export const FormCancerOne: React.FC<Props> = ({
  formValues,
  setFormValues,
  isEdit,
  isLoading,
}) => {
  const selectedDiag =
    formValues.diagnosis === "M"
      ? options[0]
      : formValues.diagnosis === "B"
      ? options[1]
      : "";
  return (
    <form className="bg-white p-4 rounded-lg">
      <h1 className="font-semibold">Tambah Data Beast Cancer</h1>
      <div className="grid grid-cols-3 gap-4 pt-2">
        <Dropdown
          name="diagnosis"
          label="Diagnosis"
          value={[selectedDiag as { label: string; value: string }]}
          onChange={(ops) => {
            setFormValues({
              ...formValues,
              diagnosis: ops === null ? "" : (ops.value as string),
            });
          }}
          options={options}
          placeholder="Diagnosis..."
          isLoading={isLoading}
        />
        {isEdit ? (
          <>
            <TextInput
              label="Chi Square"
              placeholder="Chi Square"
              type="text"
              name="chi_square"
              isDisabled
              value={formValues.chi_square}
              isLoading={isLoading}
            />
            <TextInput
              label="Forward"
              placeholder="Forward"
              type="text"
              name="forward"
              isDisabled
              value={formValues.forward}
              isLoading={isLoading}
            />
            <TextInput
              label="Backward"
              placeholder="Backward"
              type="text"
              name="backward"
              isDisabled
              value={formValues.backward}
              isLoading={isLoading}
            />
            <TextInput
              label="KNN"
              placeholder="KNN"
              type="text"
              name="knn"
              isDisabled
              value={formValues.knn}
              isLoading={isLoading}
            />
          </>
        ) : null}
        <TextInput
          label="Rata-rata radius"
          placeholder="0"
          type="text"
          name="radius_mean"
          value={formValues.radius_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, radius_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Rata-rata tekstur"
          placeholder="0"
          type="text"
          name="texture_mean"
          value={formValues.texture_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, texture_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Keliling rata-rata"
          placeholder="0"
          type="text"
          name="perimeter_mean"
          value={formValues.perimeter_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, perimeter_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Luas rata-rata"
          placeholder="0"
          type="text"
          name="area_mean"
          value={formValues.area_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, area_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Kelancaran rata-rata"
          placeholder="0"
          type="text"
          name="smoothness_mean"
          value={formValues.smoothness_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, smoothness_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Kekompakan rata-rata"
          placeholder="0"
          type="text"
          name="compactness_mean"
          value={formValues.compactness_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, compactness_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Cekungan rata-rata"
          placeholder="0"
          type="text"
          name="concavity_mean"
          value={formValues.concavity_mean}
          onChange={(value) =>
            setFormValues({ ...formValues, concavity_mean: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Titik cekung rata-rata"
          placeholder="0"
          type="text"
          name="concave_points_mean"
          value={formValues.concave_points_mean}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              concave_points_mean: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Simetri rata-rata"
          placeholder="0"
          type="text"
          name="symmetry_mean"
          value={formValues.symmetry_mean}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              symmetry_mean: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Dimensi fraktal rata-rata"
          placeholder="0"
          type="text"
          name="fractal_dimension_mean"
          value={formValues.fractal_dimension_mean}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              fractal_dimension_mean: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
      </div>
    </form>
  );
};
