"use client";

import React from "react";
import TextInput from "@/components/TextInput";

type Props = {
  formValues: CancerFormPayload;
  setFormValues: (value: CancerFormPayload) => void;
  isLoading?: boolean;
};

export const FormCancerThree: React.FC<Props> = ({
  formValues,
  setFormValues,
  isLoading,
}) => {
  return (
    <form className="bg-white p-4 rounded-lg">
      <h1 className="font-semibold">Tambah Data Beast Cancer</h1>
      <div className="grid grid-cols-3 gap-4 pt-2">
        <TextInput
          label="Radius terburuk"
          placeholder="0"
          type="text"
          name="radius_worst"
          value={formValues.radius_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, radius_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Tekstur terburuk"
          placeholder="0"
          type="text"
          name="texture_worst"
          value={formValues.texture_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, texture_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Keliling terburuk"
          placeholder="0"
          type="text"
          name="perimeter_worst"
          value={formValues.perimeter_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, perimeter_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Luas terburuk"
          placeholder="0"
          type="text"
          name="area_worst"
          value={formValues.area_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, area_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Kelancaran terburuk"
          placeholder="0"
          type="text"
          name="smoothness_worst"
          value={formValues.smoothness_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, smoothness_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Kekompakan terburuk"
          placeholder="0"
          type="text"
          name="compactness_worst"
          value={formValues.compactness_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, compactness_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Cekungan terburuk"
          placeholder="0"
          type="text"
          name="concavity_worst"
          value={formValues.concavity_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, concavity_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Titik cekung terburuk"
          placeholder="0"
          type="text"
          name="concave_points_worst"
          value={formValues.concave_points_worst}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              concave_points_worst: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Simetri terburuk"
          placeholder="0"
          type="text"
          name="symmetry_worst"
          value={formValues.symmetry_worst}
          onChange={(value) =>
            setFormValues({ ...formValues, symmetry_worst: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Fractal dimensi terburuk"
          placeholder="0"
          type="text"
          name="fractal_dimension_worst"
          value={formValues.fractal_dimension_worst}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              fractal_dimension_worst: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
      </div>
    </form>
  );
};
