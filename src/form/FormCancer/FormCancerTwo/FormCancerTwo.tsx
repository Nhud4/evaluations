"use client";

import React from "react";
import TextInput from "@/components/TextInput";

type Props = {
  formValues: CancerFormPayload;
  setFormValues: (value: CancerFormPayload) => void;
  isLoading?: boolean;
};

export const FormCancerTwo: React.FC<Props> = ({
  formValues,
  setFormValues,
  isLoading,
}) => {
  return (
    <form className="bg-white p-4 rounded-lg">
      <h1 className="font-semibold">Tambah Data Beast Cancer</h1>
      <div className="grid grid-cols-3 gap-4 pt-2">
        <TextInput
          label="Standar error radius"
          placeholder="0"
          type="text"
          name="radius_se"
          value={formValues.radius_se}
          onChange={(value) =>
            setFormValues({ ...formValues, radius_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error tekstur"
          placeholder="0"
          type="text"
          name="texture_se"
          value={formValues.texture_se}
          onChange={(value) =>
            setFormValues({ ...formValues, texture_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error keliling"
          placeholder="0"
          type="text"
          name="perimeter_se"
          value={formValues.perimeter_se}
          onChange={(value) =>
            setFormValues({ ...formValues, perimeter_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error luas"
          placeholder="0"
          type="text"
          name="area_se"
          value={formValues.area_se}
          onChange={(value) =>
            setFormValues({ ...formValues, area_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error kelancaran"
          placeholder="0"
          type="text"
          name="smoothness_se"
          value={formValues.smoothness_se}
          onChange={(value) =>
            setFormValues({ ...formValues, smoothness_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error kekompakan"
          placeholder="0"
          type="text"
          name="compactness_se"
          value={formValues.compactness_se}
          onChange={(value) =>
            setFormValues({ ...formValues, compactness_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error cekungan"
          placeholder="0"
          type="text"
          name="concavity_se"
          value={formValues.concavity_se}
          onChange={(value) =>
            setFormValues({ ...formValues, concavity_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error titik cekung"
          placeholder="0"
          type="text"
          name="concave_points_se"
          value={formValues.concave_points_se}
          onChange={(value) =>
            setFormValues({ ...formValues, concave_points_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error simetri"
          placeholder="0"
          type="text"
          name="symmetry_se"
          value={formValues.symmetry_se}
          onChange={(value) =>
            setFormValues({ ...formValues, symmetry_se: value as string })
          }
          isLoading={isLoading}
          onlyNumber
        />
        <TextInput
          label="Standar error dimensi fractal"
          placeholder="0"
          type="text"
          name="fractal_dimension_se"
          value={formValues.fractal_dimension_se}
          onChange={(value) =>
            setFormValues({
              ...formValues,
              fractal_dimension_se: value as string,
            })
          }
          isLoading={isLoading}
          onlyNumber
        />
      </div>
    </form>
  );
};
