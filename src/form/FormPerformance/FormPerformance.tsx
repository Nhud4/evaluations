"use client";

import React, { useContext, useState } from "react";
import Dropdown from "@/components/Dropdown";
import Button from "@/components/Button";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import { useMutation } from "@tanstack/react-query";
import Spinner from "@/components/Spinner";
import { toast } from "react-toastify";

const options = [
  { label: "Prostate Cancer", value: "prostate" },
  { label: "Beast Cancer", value: "beast" },
];

const defaultValue = {
  dataType: "",
};

type Props = {
  handleSuccess: () => void;
};

export const FormPerformance = ({ handleSuccess }: Props) => {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValue);

  const { mutate, isPending } = useMutation({
    mutationKey: ["performance/count"],
    mutationFn: (body: { dataType: string }) => {
      return fetch("/api/performance/count", {
        method: "POST",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        handleSuccess();
        toast.success("Berhasil menghitung performance");
      } else {
        toast.error("Gagal menghitung performance");
      }
    },
    onError: () => {
      toast.error("Gagal menghitung performance");
    },
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    mutate(formValue);
  };

  return (
    <form onSubmit={onSubmit}>
      <Dropdown
        name="data"
        label="Jenis Data"
        options={options}
        onChange={(ops) => {
          setFormValue({
            ...formValue,
            dataType: ops === null ? "" : (ops.value as string),
          });
        }}
      />
      <div className="flex justify-end items-center space-x-2 w-full mt-4">
        <Button
          className="!bg-white !border !border-primary !text-primary text-sm !px-4 !w-fit"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button
          className="text-sm !px-4 border border-primary !w-fit"
          type="submit"
        >
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
};
