"use client";

import React, { useContext, useEffect, useState } from "react";
import TextInput from "@/components/TextInput";
import Dropdown from "@/components/Dropdown";
import Button from "@/components/Button";
import { ModalContext } from "@/context/ModalContext/ModalContext";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-toastify";
import Spinner from "@/components/Spinner";

const defaultValue = {
  diagnosis: [] as { value: string | number | boolean; label: string }[],
  radius: "",
  texture: "",
  perimeter: "",
  area: "",
  smoothness: "",
  compactness: "",
  symmetry: "",
  fractal_dimension: "",
  chi_square: "",
  forward: "",
  backward: "",
  knn: "",
};

const options = [
  { label: "Kangker Ganas (M)", value: "M" },
  { label: "Kangker Jinak (B)", value: "B" },
];

type Props = {
  data?: Prostate;
  formType: "add" | "edit" | "detail";
  handleSuccess: () => void;
};

export const FormProstate: React.FC<Props> = ({
  data,
  formType,
  handleSuccess,
}) => {
  const { onClose } = useContext(ModalContext);
  const [formValues, setFormValues] = useState(defaultValue);

  useEffect(() => {
    if (data) {
      setFormValues({
        ...defaultValue,
        ...data,
        diagnosis: data?.diagnosis === "M" ? [options[0]] : [options[1]],
      });
    }
  }, [data]);

  const { mutate: AddMutate, isPending } = useMutation({
    mutationKey: ["prostate/add"],
    mutationFn: (body: ProstatePayload) => {
      return fetch("/api/data/create/prostate", {
        method: "POST",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        handleSuccess();
        toast.success("Berhasil menambahkan data prostate");
      } else {
        toast.error("Gagal menambahkan data prostate");
      }
    },
    onError: () => {
      toast.error("Gagal menambahkan data prostate");
    },
  });

  const { mutate: EditMutate, isPending: editLoading } = useMutation({
    mutationKey: ["prostate/edit"],
    mutationFn: (body: ProstatePayload) => {
      return fetch(`/api/data/update/prostate/${data?.code}`, {
        method: "PUT",
        body: JSON.stringify(body),
      });
    },
    onSuccess: (data) => {
      if (data.ok) {
        handleSuccess();
        toast.success("Berhasil merubah data prostate");
      } else {
        toast.error("Gagal merubah data prostate");
      }
    },
    onError: () => {
      toast.error("Gagal merubah data prostate");
    },
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const payload = {
      ...formValues,
      diagnosis: formValues.diagnosis[0].value as string,
    };
    if (formType === "add") {
      AddMutate(payload);
    } else {
      EditMutate(payload);
    }
  };

  const isDisabled = Boolean(formType === "detail");
  const checkValue = Boolean(
    formValues.diagnosis &&
      formValues.radius &&
      formValues.texture &&
      formValues.perimeter &&
      formValues.area &&
      formValues.smoothness &&
      formValues.compactness &&
      formValues.symmetry &&
      formValues.fractal_dimension
  );

  return (
    <form className="grid grid-cols-3 gap-4" onSubmit={onSubmit}>
      <div className="col-span-3">
        <Dropdown
          label="Diagnosis"
          name="diagnosis"
          isDisabled={isDisabled}
          value={formValues.diagnosis}
          onChange={(ops) => {
            setFormValues({
              ...formValues,
              diagnosis: ops === null ? [] : [ops],
            });
          }}
          options={options}
        />
      </div>
      <TextInput
        label="Radius"
        placeholder="0"
        type="text"
        name="radius"
        isDisabled={isDisabled}
        value={formValues.radius}
        onChange={(value) =>
          setFormValues({ ...formValues, radius: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Texture"
        placeholder="0"
        type="text"
        name="texture"
        isDisabled={isDisabled}
        value={formValues.texture}
        onChange={(value) =>
          setFormValues({ ...formValues, texture: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Perimeter"
        placeholder="0"
        type="text"
        name="perimeter"
        isDisabled={isDisabled}
        value={formValues.perimeter}
        onChange={(value) =>
          setFormValues({ ...formValues, perimeter: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Area"
        placeholder="0"
        type="text"
        name="area"
        isDisabled={isDisabled}
        value={formValues.area}
        onChange={(value) =>
          setFormValues({ ...formValues, area: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Smoothness"
        placeholder="0"
        type="text"
        name="smoothness"
        isDisabled={isDisabled}
        value={formValues.smoothness}
        onChange={(value) =>
          setFormValues({ ...formValues, smoothness: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Compactness"
        placeholder="0"
        type="text"
        name="compactness"
        isDisabled={isDisabled}
        value={formValues.compactness}
        onChange={(value) =>
          setFormValues({ ...formValues, compactness: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Symmetry"
        placeholder="0"
        type="text"
        name="symmetry"
        isDisabled={isDisabled}
        value={formValues.symmetry}
        onChange={(value) =>
          setFormValues({ ...formValues, symmetry: value as string })
        }
        onlyNumber
      />
      <TextInput
        label="Fractal Dimension"
        placeholder="0"
        type="text"
        name="fractal_dimension"
        isDisabled={isDisabled}
        value={formValues.fractal_dimension}
        onChange={(value) =>
          setFormValues({ ...formValues, fractal_dimension: value as string })
        }
        onlyNumber
      />
      {formType !== "add" ? (
        <>
          <TextInput
            label="Chi Square"
            placeholder="0"
            type="text"
            name="chi_square"
            isDisabled
            value={formValues.chi_square}
          />
          <TextInput
            label="Forward"
            placeholder="0"
            type="text"
            name="forward"
            isDisabled
            value={formValues.forward}
          />
          <TextInput
            label="Backward"
            placeholder="0"
            type="text"
            name="backward"
            isDisabled
            value={formValues.backward}
          />
          <TextInput
            label="KNN"
            placeholder="0"
            type="text"
            name="knn"
            isDisabled
            value={formValues.knn}
          />
        </>
      ) : null}
      {!isDisabled ? (
        <div className="flex items-center space-x-2 col-span-3 justify-self-end">
          <Button
            className="!bg-white !border !border-primary !text-primary !px-4"
            onClick={onClose}
          >
            Batal
          </Button>
          <Button
            className="!px-4 border"
            type="submit"
            isDisabled={!checkValue}
          >
            {isPending || editLoading ? <Spinner /> : "Konfirmasi"}
          </Button>
        </div>
      ) : null}
    </form>
  );
};
