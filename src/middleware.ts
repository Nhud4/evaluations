import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { cookies } from "next/headers";

export function middleware(request: NextRequest) {
  const { pathname } = request.nextUrl;
  const cookie = cookies();
  const token = cookie.get("token")?.value;
  const protectedPaths = [
    "/dashboard",
    "/pusat-data/prostate-cancer",
    "/pusat-data/breast-cancer",
    "/performance",
  ];

  const isLoggedIn = pathname.startsWith("/login") && token;
  const isProtectedPath = protectedPaths.some((path) => pathname === path);

  const isRequireAuth = isProtectedPath && !token;

  if (isRequireAuth) {
    return NextResponse.redirect(new URL(`/login`, request.url));
  }

  if (!isLoggedIn && pathname === "/") {
    return NextResponse.redirect(new URL("/login", request.url));
  }

  if (isLoggedIn && pathname === "/") {
    return NextResponse.redirect(new URL("/dashboard", request.url));
  }

  return NextResponse.next();
}

export const config = {
  matcher: [
    "/",
    "/login",
    "/dashboard",
    "/pusat-data/prostate-cancer",
    "/pusat-data/breast-cancer",
    "/performance",
  ],
};
