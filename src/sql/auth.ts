import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const MigrationAuth = async () => {
  try {
    const sql = `CREATE TABLE IF NOT EXISTS admin (
      id INT PRIMARY KEY,
      name VARCHAR(255),
      username VARCHAR(255),
      email VARCHAR(255),
      password	VARCHAR(255)
    )`;
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
