import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const MigrationCancer = async () => {
  try {
    const sql = `CREATE TABLE IF NOT EXISTS cancer (
      id INT NULL AUTO_INCREMENT,
      code VARCHAR(255),
      diagnosis	VARCHAR(255),
      radius_mean	VARCHAR(255),
      texture_mean VARCHAR(255),
      perimeter_mean VARCHAR(255),
      area_mean	VARCHAR(255),
      smoothness_mean	VARCHAR(255),
      compactness_mean VARCHAR(255),
      concavity_mean VARCHAR(255),
      concave_points_mean	VARCHAR(255),
      symmetry_mean	VARCHAR(255),
      fractal_dimension_mean VARCHAR(255),
      radius_se	VARCHAR(255),
      texture_se VARCHAR(255),
      perimeter_se VARCHAR(255),
      area_se	VARCHAR(255),
      smoothness_se	VARCHAR(255),
      compactness_se VARCHAR(255),
      concavity_se VARCHAR(255),
      concave_points_se	VARCHAR(255),
      symmetry_se	VARCHAR(255),
      fractal_dimension_se VARCHAR(255),
      radius_worst VARCHAR(255),
      texture_worst	VARCHAR(255),
      perimeter_worst	VARCHAR(255),
      area_worst VARCHAR(255),
      smoothness_worst VARCHAR(255),
      compactness_worst	VARCHAR(255),
      concavity_worst	VARCHAR(255),
      concave_points_worst VARCHAR(255),
      symmetry_worst VARCHAR(255),
      fractal_dimension_worst	VARCHAR(255),
      chi_square VARCHAR(255),
      forward VARCHAR(255),
      backward VARCHAR(255),
      PRIMARY KEY (id)
    )`;
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
