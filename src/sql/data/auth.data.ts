import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const GetById = async (id: number) => {
  try {
    const sql = "SELECT * FROM admin WHERE id = ?";
    const value = [id];
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const GetByUsername = async (username: string) => {
  try {
    const sql = "SELECT * FROM admin WHERE username = ?";
    const value = [username];
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const InsertAuthData = async (payload: AuthPayload) => {
  try {
    const sql = `INSERT INTO admin (name, email, username, password) VALUES (?, ?, ?, ?)`;
    const value = [
      payload.name,
      payload.email,
      payload.username,
      payload.password,
    ];
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const UpdateAuthData = async (payload: AuthPayload, id: number) => {
  try {
    let sql = `UPDATE admin SET name = ?, email = ?, username = ?`;
    const value = [payload.name, payload.email, payload.username];

    if (payload.password) {
      sql += ", password = ?";
      value.push(payload.password);
    }

    if (id) {
      sql += " WHERE id = ?";
      value.push(id.toString());
    }

    const result = await query(sql, value);

    return result;
  } catch (error) {
    return error;
  }
};
