import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const GetCancerByCode = async (code: string) => {
  try {
    const sql = "SELECT * FROM cancer WHERE code = ?";
    const value = [code];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const GetCancer = async (params: DataParam) => {
  try {
    let sql = "SELECT * FROM cancer";
    if (params.search) {
      sql += ` WHERE code LIKE '%${params.search}%'`;
    }
    sql += " LIMIT ? OFFSET ?";
    const value = [params.limit, params.offset];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const CountCancer = async (params: DataParam) => {
  try {
    let sql = "SELECT COUNT(id) as total FROM cancer";
    if (params.search) {
      sql += ` WHERE code LIKE '%${params.search}%'`;
    }

    const value = [params.limit, params.offset];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const InsertCancer = async (payload: CancerPayload) => {
  try {
    const sql = `INSERT INTO cancer (
      code,
      diagnosis,
      radius_mean,
      texture_mean,
      perimeter_mean,
      area_mean,
      smoothness_mean,
      compactness_mean,
      concavity_mean,
      concave_points_mean,
      symmetry_mean,
      fractal_dimension_mean,
      radius_se,
      texture_se,
      perimeter_se,
      area_se,
      smoothness_se,
      compactness_se,
      concavity_se,
      concave_points_se,
      symmetry_se,
      fractal_dimension_se,
      radius_worst,
      texture_worst,
      perimeter_worst,
      area_worst,
      smoothness_worst,
      compactness_worst,
      concavity_worst,
      concave_points_worst,
      symmetry_worst,
      fractal_dimension_worst,
      chi_square,
      forward,
      backward,
      knn
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    const value = [
      payload.code,
      payload.diagnosis,
      payload.radius_mean,
      payload.texture_mean,
      payload.perimeter_mean,
      payload.area_mean,
      payload.smoothness_mean,
      payload.compactness_mean,
      payload.concavity_mean,
      payload.concave_points_mean,
      payload.symmetry_mean,
      payload.fractal_dimension_mean,
      payload.radius_se,
      payload.texture_se,
      payload.perimeter_se,
      payload.area_se,
      payload.smoothness_se,
      payload.compactness_se,
      payload.concavity_se,
      payload.concave_points_se,
      payload.symmetry_se,
      payload.fractal_dimension_se,
      payload.radius_worst,
      payload.texture_worst,
      payload.perimeter_worst,
      payload.area_worst,
      payload.smoothness_worst,
      payload.compactness_worst,
      payload.concavity_worst,
      payload.concave_points_worst,
      payload.symmetry_worst,
      payload.fractal_dimension_worst,
      payload.chi_square,
      payload.forward,
      payload.backward,
      payload.knn,
    ];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const UpdateCancer = async (payload: CancerPayload) => {
  try {
    const sql = `UPDATE cancer SET
      diagnosis = ?,
      radius_mean = ?,
      texture_mean = ?,
      perimeter_mean = ?,
      area_mean = ?,
      smoothness_mean = ?,
      compactness_mean = ?,
      concavity_mean = ?,
      concave_points_mean = ?,
      symmetry_mean = ?,
      fractal_dimension_mean = ?,
      radius_se = ?,
      texture_se = ?,
      perimeter_se = ?,
      area_se = ?,
      smoothness_se = ?,
      compactness_se = ?,
      concavity_se = ?,
      concave_points_se = ?,
      symmetry_se = ?,
      fractal_dimension_se = ?,
      radius_worst = ?,
      texture_worst = ?,
      perimeter_worst = ?,
      area_worst = ?,
      smoothness_worst = ?,
      compactness_worst = ?,
      concavity_worst = ?,
      concave_points_worst = ?,
      symmetry_worst = ?,
      fractal_dimension_worst = ?,
      chi_square = ?,
      forward = ?,
      backward = ?,
      knn = ?
      WHERE code = ?`;
    const value = [
      payload.diagnosis,
      payload.radius_mean,
      payload.texture_mean,
      payload.perimeter_mean,
      payload.area_mean,
      payload.smoothness_mean,
      payload.compactness_mean,
      payload.concavity_mean,
      payload.concave_points_mean,
      payload.symmetry_mean,
      payload.fractal_dimension_mean,
      payload.radius_se,
      payload.texture_se,
      payload.perimeter_se,
      payload.area_se,
      payload.smoothness_se,
      payload.compactness_se,
      payload.concavity_se,
      payload.concave_points_se,
      payload.symmetry_se,
      payload.fractal_dimension_se,
      payload.radius_worst,
      payload.texture_worst,
      payload.perimeter_worst,
      payload.area_worst,
      payload.smoothness_worst,
      payload.compactness_worst,
      payload.concavity_worst,
      payload.concave_points_worst,
      payload.symmetry_worst,
      payload.fractal_dimension_worst,
      payload.chi_square,
      payload.forward,
      payload.backward,
      payload.knn,
      payload.code,
    ];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const DeleteCancer = async (code: string) => {
  try {
    const sql = "DELETE FROM cancer WHERE code = ?";
    const value = [code];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const GetPerformanceBeast = async () => {
  try {
    const sql = `SELECT code, diagnosis, chi_square, forward, backward, knn 
    FROM cancer`;
    const value: never[] = [];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const InsertBulkCancer = async (payload: any[]) => {
  try {
    const sql = `INSERT INTO cancer (
      code,
      diagnosis,
      radius_mean,
      texture_mean,
      perimeter_mean,
      area_mean,
      smoothness_mean,
      compactness_mean,
      concavity_mean,
      concave_points_mean,
      symmetry_mean,
      fractal_dimension_mean,
      radius_se,
      texture_se,
      perimeter_se,
      area_se,
      smoothness_se,
      compactness_se,
      concavity_se,
      concave_points_se,
      symmetry_se,
      fractal_dimension_se,
      radius_worst,
      texture_worst,
      perimeter_worst,
      area_worst,
      smoothness_worst,
      compactness_worst,
      concavity_worst,
      concave_points_worst,
      symmetry_worst,
      fractal_dimension_worst,
      chi_square,
      forward,
      backward,
      knn
      ) VALUES ?`;
    const value = [payload];

    const db = await pool.getConnection();
    const format = await db.format(sql, value);
    const [rows] = await db.execute(format);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const CountAllBeast = async () => {
  try {
    const sql = "SELECT COUNT(id) as total FROM cancer";
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
