import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const format = await db.format(sql, value);
    const [rows] = await db.execute(format);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const ListPerformance = async (param: string) => {
  try {
    const sql = "SELECT * FROM performance WHERE data_type = ?";
    const value = [param];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const InsertPerformance = async (payload: any[]) => {
  try {
    const sql = `INSERT INTO performance (data_type, algo, recall, pres, accuracy, f1_score) VALUES ?`;
    const value = [payload];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const DeletePerformance = async (payload: string) => {
  try {
    const sql = "DELETE FROM performance WHERE data_type = ?";
    const value = [payload];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const ListAllPerformance = async () => {
  try {
    const sql = "SELECT data_type, algo, accuracy FROM performance";
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
