import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const GetProstateByCode = async (code: string) => {
  try {
    const sql = "SELECT * FROM prostate WHERE code = ?";
    const value = [code];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const GetProstate = async (params: DataParam) => {
  try {
    let sql = "SELECT * FROM prostate";
    if (params.search) {
      sql += ` WHERE code LIKE '%${params.search}%'`;
    }
    sql += " LIMIT ? OFFSET ?";
    const value = [params.limit, params.offset];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const CountProstate = async (params: DataParam) => {
  try {
    let sql = "SELECT COUNT(id) as total FROM prostate";
    if (params.search) {
      sql += ` WHERE code LIKE '%${params.search}%'`;
    }

    const value = [params.limit, params.offset];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const InsertProstate = async (payload: ProstatePayload) => {
  try {
    const sql = `INSERT INTO prostate (
    code,
    diagnosis,
    radius,
    texture,
    perimeter,
    area,
    smoothness,
    compactness,
    symmetry,
    fractal_dimension,
    chi_square,
    forward,
    backward,
    knn
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;

    const value = [
      payload.code,
      payload.diagnosis,
      payload.radius,
      payload.texture,
      payload.perimeter,
      payload.area,
      payload.smoothness,
      payload.compactness,
      payload.symmetry,
      payload.fractal_dimension,
      payload.chi_square,
      payload.forward,
      payload.backward,
      payload.knn,
    ];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const UpdateProstate = async (payload: ProstatePayload) => {
  try {
    const sql = `UPDATE prostate SET 
    diagnosis = ?,
    radius = ?,
    texture = ?,
    perimeter = ?,
    area = ?,
    smoothness = ?,
    compactness = ?,
    symmetry = ?,
    fractal_dimension = ?,
    chi_square = ?,
    forward = ?,
    backward = ?,
    knn = ?
    WHERE code = ?`;

    const value = [
      payload.diagnosis,
      payload.radius,
      payload.texture,
      payload.perimeter,
      payload.area,
      payload.smoothness,
      payload.compactness,
      payload.symmetry,
      payload.fractal_dimension,
      payload.chi_square,
      payload.forward,
      payload.backward,
      payload.knn,
      payload.code,
    ];

    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const DeleteProstate = async (code: string) => {
  try {
    const sql = "DELETE FROM prostate WHERE code = ?";
    const value = [code];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const GetPerformanceProstate = async () => {
  try {
    const sql = `SELECT code, diagnosis, chi_square, forward, backward, knn 
    FROM prostate`;
    const value: never[] = [];
    const result = await query(sql, value);
    return result;
  } catch (error) {
    return error;
  }
};

export const InsertBulkProstate = async (payload: any[]) => {
  try {
    const sql = `INSERT INTO prostate (
    code,
    diagnosis,
    radius,
    texture,
    perimeter,
    area,
    smoothness,
    compactness,
    symmetry,
    fractal_dimension,
    chi_square,
    forward,
    backward,
    knn
    ) VALUES ?`;
    const value = [payload];

    const db = await pool.getConnection();
    const format = await db.format(sql, value);
    const [rows] = await db.execute(format);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const CountAllProstate = async () => {
  try {
    const sql = "SELECT COUNT(id) as total FROM prostate";
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
