type AuthPayload = {
  name: string;
  username: string;
  email: string;
  password?: string;
};

type DataParam = {
  limit: number;
  offset: number;
  search?: string;
};

type ApplePayload = {
  code?: string;
  size: string;
  weight: string;
  sweetness: string;
  crunchiness: string;
  juiciness: string;
  ripeness: string;
  acidity: string;
  quality: string;
};

type CancerPayload = {
  code?: string;
  diagnosis: string;
  radius_mean: string;
  texture_mean: string;
  perimeter_mean: string;
  area_mean: string;
  smoothness_mean: string;
  compactness_mean: string;
  concavity_mean: string;
  concave_points_mean: string;
  symmetry_mean: string;
  fractal_dimension_mean: string;
  radius_se: string;
  texture_se: string;
  perimeter_se: string;
  area_se: string;
  smoothness_se: string;
  compactness_se: string;
  concavity_se: string;
  concave_points_se: string;
  symmetry_se: string;
  fractal_dimension_se: string;
  radius_worst: string;
  texture_worst: string;
  perimeter_worst: string;
  area_worst: string;
  smoothness_worst: string;
  compactness_worst: string;
  concavity_worst: string;
  concave_points_worst: string;
  symmetry_worst: string;
  fractal_dimension_worst: string;
  chi_square?: string;
  forward?: string;
  backward?: string;
  knn?: string;
};

type ProstatePayload = {
  code?: string;
  diagnosis: string;
  radius: string;
  texture: string;
  perimeter: string;
  area: string;
  smoothness: string;
  compactness: string;
  symmetry: string;
  fractal_dimension: string;
  chi_square?: string;
  forward?: string;
  backward?: string;
  knn?: string;
};

type PerformancePayload = {
  algo: string;
  recall: string;
  pres: string;
  accuracy: string;
  f1Score: string;
};
