import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const MigrationPerformance = async () => {
  try {
    const sql = `CREATE TABLE IF NOT EXISTS performance (
    id INT NULL AUTO_INCREMENT,
    data_type VARCHAR(255), 
    algo VARCHAR(255),
    recall VARCHAR(255),
    pres VARCHAR(255),
    accuracy VARCHAR(255),
    f1_score VARCHAR(255),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (id)
    )`;
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
