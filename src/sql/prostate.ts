import pool from "@/configs/mysql";

const query = async (sql: string, value: any) => {
  try {
    const db = await pool.getConnection();
    const [rows] = await db.execute(sql, value);
    db.release();

    return rows;
  } catch (error) {
    return error;
  }
};

export const MigrationProstate = async () => {
  try {
    const sql = `CREATE TABLE IF NOT EXISTS prostate (
      id INT NOT NULL AUTO_INCREMENT,
      code VARCHAR(255),
      diagnosis VARCHAR(255),
      radius VARCHAR(255),
      texture VARCHAR(255),
      perimeter VARCHAR(255),
      area VARCHAR(255),
      smoothness VARCHAR(255),
      compactness VARCHAR(255),
      symmetry VARCHAR(255),
      fractal_dimension VARCHAR(255),
      knn VARCHAR(255),
      chi_square VARCHAR(255),
      forward VARCHAR(255),
      backward VARCHAR(255),
      PRIMARY KEY (id)
    )`;
    const result = await query(sql, []);
    return result;
  } catch (error) {
    return error;
  }
};
