type Actions = "detail" | "edit" | "delete" | "status";

type ApplePayload = {
  size: string;
  weight: string;
  sweetness: string;
  crunchiness: string;
  juiciness: string;
  ripeness: string;
  acidity: string;
  quality: string;
  code?: string;
};

type TableParams = {
  page: number;
  size: number;
  search?: string;
};
