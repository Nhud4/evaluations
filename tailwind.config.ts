import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/feature/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/form/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
    colors: {
      white: "#FFFF",
      black: "#171717",
      primary: {
        1: "#2684FF",
        DEFAULT: "#1CA742",
      },
      secondary: {
        1: "#40A5A1",
        2: "#AC4980",
        3: "#F5F5F5",
        4: "#525252",
        5: "#B8646B",
        6: "#B3A742",
        DEFAULT: "#1C73A7",
      },
      neutral: {
        2: "#DFEAF2",
        3: "#CCCCCC",
        4: "#E2E8F0",
        5: "#828282",
        6: "#EFEFEF",
        7: "#404040",
        DEFAULT: "#E5E5E5",
      },
      danger: {
        50: "#FBEAE9",
        500: "#D62A24",
        DEFAULT: "#F26464",
      },
    },
  },
  plugins: [],
};
export default config;
